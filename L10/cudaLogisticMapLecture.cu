
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void logisticMapKernelV0(int N, int Niter, float *x, float rmin, float rmax){

  int t = threadIdx.x + blockIdx.x*blockDim.x;

  float r = rmin + (rmax-rmin)*t/(float)N;

  float xn = 0.5f;

  for(int n=0;n<Niter;++n){
    xn = r*xn*(1.f-xn);
  }

  if(t<N)
    x[t] = xn;
}


__global__ void logisticMapKernelV1(int N, int Niter, float *x, float rmin, float rmax){

  int t = threadIdx.x + blockIdx.x*blockDim.x;

  float r = rmin + (rmax-rmin)*t/(float)N;

  float xn = 0.25f;

  // a = 1/2, b = -1/r => x = 1/2 - y/r
  float yn = r*(0.5f-xn);
  const float cn = 0.5f*r - 0.25f*r*r;

#pragma unroll 1
  for(int n=0;n<Niter;++n){
    //    xn = r*xn*(1.f-xn);
    yn = cn + yn*yn;
  }
  xn = 0.5f - yn/r;

  if(t<N)
    x[t] = xn;
}

#define p_Niter 1000
__global__ void logisticMapKernelV2(int N, float *x, float rmin, float rmax){

  int t = threadIdx.x + blockIdx.x*blockDim.x;

  float r = rmin + (rmax-rmin)*t/(float)N;

  float xn = 0.25f;

  // a = 1/2, b = -1/r => x = 1/2 - y/r
  float yn = r*(0.5f-xn);
  const float cn = 0.5f*r - 0.25f*r*r;

#pragma unroll 250
  for(int n=0;n<p_Niter;++n){
    //    xn = r*xn*(1.f-xn);
    yn = cn + yn*yn;
  }
  xn = 0.5f - yn/r;

  if(t<N)
    x[t] = xn;
}


int main(int argc, char **argv){

  int N = atoi(argv[1]);
  int Niter = atoi(argv[2]);

  float rmin = 0.01;
  float rmax = 4;

  float *c_x, *h_x;

  cudaMalloc(&c_x, N*sizeof(float));
  
  int T = 256;
  int G = (N+T-1)/T;

  cudaEvent_t tic0, toc0, tic1, toc1, tic2, toc2;

  cudaEventCreate(&tic0);
  cudaEventCreate(&toc0);
  cudaEventCreate(&tic1);
  cudaEventCreate(&toc1);
  cudaEventCreate(&tic2);
  cudaEventCreate(&toc2);

  cudaEventRecord(tic0); 
  logisticMapKernelV0 <<< G, T >>> (N, Niter, c_x, rmin, rmax);
  cudaEventRecord(toc0);
  cudaEventRecord(tic1);
  logisticMapKernelV1 <<< G, T >>> (N, Niter, c_x, rmin, rmax);
  cudaEventRecord(toc1);
  cudaEventRecord(tic2);
  logisticMapKernelV2 <<< G, T >>> (N, c_x, rmin, rmax);
  cudaEventRecord(toc2);
  
  h_x = (float*) calloc(N, sizeof(float));
  cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyDeviceToHost);


  float elapsed0, elapsed1, elapsed2;
  cudaEventElapsedTime(&elapsed0, tic0, toc0);
  cudaEventElapsedTime(&elapsed1, tic1, toc1);
  cudaEventElapsedTime(&elapsed2, tic2, toc2);

  elapsed0 /= 1000.f;
  elapsed1 /= 1000.f;
  elapsed2 /= 1000.f;

  printf("V0 took %e s\n", elapsed0);
  printf("V1 took %e s\n", elapsed1);
  printf("V2 took %e s\n", elapsed2);

  float tflops = (2.*Niter)*N/1.e12;

  printf("V0 go %e TFLOPS/s\n", tflops/elapsed0);
  printf("V1 go %e TFLOPS/s\n", tflops/elapsed1);
  printf("V2 go %e TFLOPS/s\n", tflops/elapsed2);
  
}
