
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void logisticMapKernelV0(int N, int Niter, float *x, float rmin, float rmax){

  int t = threadIdx.x + blockIdx.x*blockDim.x;

  float r = rmin + (rmax-rmin)*t/(float)N;

  float xn = x[t];

  for(int n=0;n<Niter;++n){
    xn = r*xn*(1.f-xn);
  }

  if(t<N)
    x[t] = xn;
}

__global__ void logisticMapKernelV1(int N, int Niter, float *x, float rmin, float rmax){

  int t = threadIdx.x + blockIdx.x*blockDim.x;

  float r = rmin + (rmax-rmin)*t/(float)N;

  float xn = x[t];
  float yn = r*(0.5-xn);
  float cn = -0.25f*r*r + 0.5f*r;

#pragma unroll 1
  for(int n=0;n<Niter;++n){
    yn = cn + yn*yn;
  }
  xn = 0.5f-yn/r;
  
  if(t<N)
    x[t] = xn;
}


#define p_Niter 1000
#define p_T 256

__global__ void __launch_bounds__(p_T) logisticMapKernelV2(int N, float *x, float rmin, float rmax){

  int t = threadIdx.x + blockIdx.x*blockDim.x;

  float r = rmin + (rmax-rmin)*t/(float)N;

  float xn = x[t];
  float yn = r*(0.5-xn);
  float cn = -0.25f*r*r + 0.5f*r;

#pragma unroll 100
  for(int n=0;n<p_Niter;++n){
    yn = cn + yn*yn;
  }
  xn = 0.5f-yn/r;
  
  if(t<N)
    x[t] = xn;
}


int main(int argc, char **argv){

  int N = atoi(argv[1]);
  int Niter = atoi(argv[2]);

  float rmin = 0.01;
  float rmax = 4;

  float *c_x, *h_x;
  h_x = (float*) calloc(N, sizeof(float));
  
  cudaMalloc(&c_x, N*sizeof(float));

  for(int t=0;t<N;++t){
    h_x[t] = drand48();
  }

  int T = p_T;
  int G = (N+p_T-1)/p_T;

  cudaEvent_t tic0, toc0;
  cudaEvent_t tic1, toc1;
  cudaEvent_t tic2, toc2;

  cudaEventCreate(&tic0);
  cudaEventCreate(&tic1);		
  cudaEventCreate(&tic2);	
  cudaEventCreate(&toc0);
  cudaEventCreate(&toc1);		
  cudaEventCreate(&toc2);	

  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaEventRecord(tic0);  
  logisticMapKernelV0 <<< G, T >>> (N, Niter, c_x, rmin, rmax);
  cudaEventRecord(toc0);

  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaEventRecord(tic1);   
  logisticMapKernelV1 <<< G, T >>> (N, Niter, c_x, rmin, rmax);
  cudaEventRecord(toc1);
  
  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaEventRecord(tic2);   
  logisticMapKernelV2 <<< G, T >>> (N, c_x, rmin, rmax);
  cudaEventRecord(toc2);

  // this syncs HOST and DEVICE
  cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyDeviceToHost);

  
  float elapsed0, elapsed1, elapsed2;
  cudaEventElapsedTime(&elapsed0, tic0, toc0); elapsed0 /= 1000.f;
  cudaEventElapsedTime(&elapsed1, tic1, toc1); elapsed1 /= 1000.f;
  cudaEventElapsedTime(&elapsed2, tic2, toc2); elapsed2 /= 1000.f;

  double gflops = Niter*(N*2.)/1.e9;

  printf("Elapsed K0: %g, GFLOPS/s: %g\n", elapsed0, gflops/elapsed0);
  printf("Elapsed K1: %g, GFLOPS/s: %g\n", elapsed1, gflops/elapsed1);
  printf("Elapsed K2: %g, GFLOPS/s: %g\n", elapsed2, gflops/elapsed2);



}
