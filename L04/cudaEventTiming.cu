
#include <stdio.h>
#include <time.h>

__global__
void devicePrint(int N, float *x, float *y){
  
  int t  = threadIdx.x; // in the range 0 to number of threads per thread-block
  int b  = blockIdx.x;  // in the range 0 to number of blocks-1
  int d  = blockDim.x;  // how many threads in each thread-block
  int g  = gridDim.x;   // how many thread-blocks there are

  //  unique map between thread index, block index, and block size and integers
  int n = b*d + t;

  if(n<N)
    y[n] = 2.*x[n];
  
}


void hostPrint(){

  printf("Hello world from CPU!!!\n");
  
}

int main(int argc, char **argv){

  int G = atoi(argv[1]);
  int T = atoi(argv[2]);

  int N = G*T;

  float *h_x, *h_y;
  float *c_x, *c_y;

  // allocate arrays on HOST
  h_x = (float*) malloc(N*sizeof(float));

  for(int n=0;n<10;++n){
    h_x[n] = n;
  }
  
  // allocate array on DEVICE
  cudaMalloc(&c_x, N*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));

  // copy data from HOST to DEVICE
  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);

  devicePrint <<< G , T  >>> (N, c_x, c_y);

  
  // launch kernel
  clock_t tic, toc;

  cudaDeviceSynchronize();
  tic = clock();
  devicePrint <<< G , T  >>> (N, c_x, c_y);
  cudaDeviceSynchronize();
  toc = clock();
  
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  cudaEventRecord(start);
  devicePrint <<< G , T  >>> (N, c_x, c_y);
  cudaEventRecord(stop);

  cudaDeviceSynchronize();

  float deviceElapsed;
  cudaEventElapsedTime(&deviceElapsed, start, stop);
  deviceElapsed = deviceElapsed/1000.;
  
  double elapsed = (toc-tic)/CLOCKS_PER_SEC;
  printf("elapsed = %e\n",  elapsed);
  printf("deviceElapsed = %e\n",  deviceElapsed);
  
  devicePrint <<< G , T  >>> (N, c_y, c_x);
  devicePrint <<< G , T  >>> (N, c_x, c_y);

  // copy data from DEVICE to HOST
  cudaMemcpy(h_x, c_y, N*sizeof(float), cudaMemcpyDeviceToHost);
  
  for(int n=0;n<10;++n){
    printf("h_x[%d] = %f\n", n, h_x[n]);
  }

  cudaFree(c_x);
  cudaFree(c_y);

  free(h_x);
  
}
