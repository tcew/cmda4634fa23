#include <stdlib.h>
#include <stdio.h>
#include "cuda.h"

__global__ void addVectorsKernel(int N, float *c_x, float *c_y, float *c_z){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  // evaluate array index "n" for this thread
  int n = t + b*B;
  
  if(n<N){ // only add entries if "n" a legal index
    c_z[n] = c_x[n] + c_y[n];
  }

}

// HOST main function
int main(int argc, char **argv){

  if(argc<2){ printf("usage: ./cudaAddVectors N\n"); exit(-1); }
  
  // read command line argument
  int N = atoi(argv[1]);

  // allocate an array on the HOST
  float *h_x = (float*) malloc(N*sizeof(float));
  float *h_y = (float*) malloc(N*sizeof(float));
  float *h_z = (float*) malloc(N*sizeof(float));

  // fill values of h_x and h_y on HOST
  int n;
  for(n=0;n<N;++n){
    h_x[n] = drand48();
    h_y[n] = drand48();
  }
    
  // allocate an array on the DEVICE
  float *c_x, *c_y, *c_z;
  cudaMalloc(&c_x, N*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));
  cudaMalloc(&c_z, N*sizeof(float));

  // copy data from HOST to DEVICE 
  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_y, h_y, N*sizeof(float), cudaMemcpyHostToDevice);
  
  // call kernel to set values in array
  int B = 256;
  int G = (N+B-1)/B; 
  addVectorsKernel <<< G , B >>> (N,  c_x, c_y, c_z);

  // copy data from DEVICE to HOST (blocking)
  cudaMemcpy(h_z, c_z, N*sizeof(float), cudaMemcpyDeviceToHost);
  
  // print out result
  float l1Diff = 0;
  for(n=0;n<N;++n){
    float  ans = h_x[n] + h_y[n];
    float diff = fabs(h_z[n] - ans);
    l1Diff += diff;
  }

  printf("N=%d, G=%d, B=%d, G*B=%d, l1 diff=%17.15e\n", N, G, B, G*B, l1Diff);

  return 0;
}
