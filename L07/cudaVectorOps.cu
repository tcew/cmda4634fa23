
#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

__device__ int idx(){

  int n = threadIdx.x + blockIdx.x*blockDim.x;
  return n;
}

__global__ void cudaFabs(int N, float *x, float *y){

  int n = idx();
  if(n<N){
    y[n] = fabs(x[n]);
  }

}


__global__ void cudaLog(int N, float *x, float *y){

  int n = idx();
  if(n<N){
    y[n] = log(x[n]);
  }

}



__global__ void cudaExp(int N, float *x, float *y){

  int n = idx();
  if(n<N){
    y[n] = exp(x[n]);
  }

}


__global__ void cudaSqrt(int N, float *x, float *y){

  int n = idx();
  if(n<N){
    y[n] = sqrt(x[n]);
  }

}

__global__ void cudaPow(int N, float *x, float *y, float *z){

  int n = idx();
  if(n<N){
    z[n] = pow(x[n], y[n]);
  }

}

__global__ void cudaSin(int N, float *x, float *y){

  int n = idx();
  if(n<N){
    y[n] = sin(x[n]);
  }

}


__global__ void cudaTan(int N, float *x, float *y){

  int n = idx();
  if(n<N){
    y[n] = tan(x[n]);
  }

}


__global__ void cudaAtan2(int N, float *x, float *y, float *z){

  int n = idx();
  if(n<N){
    z[n] = atan2(y[n], x[n]);
  }

}



void launcher(const char *op, int N, float *c_x, float *c_y, float *c_z, size_t *bytes){

  int T = 256;
  int G = (N+T-1)/T;

  if(!strcmp(op, "fabs")){
    cudaFabs <<< G, T >>> (N, c_x, c_y);
    *bytes = N*2*sizeof(float);
  }else if(!strcmp(op, "log")){
    cudaLog <<< G, T >>> (N, c_x, c_y);
    *bytes = N*2*sizeof(float);
  }else if(!strcmp(op, "exp")){
    cudaExp <<< G, T >>> (N, c_x, c_y);
    *bytes = N*2*sizeof(float);
  } else if(!strcmp(op, "sqrt")){
    cudaSqrt <<< G, T >>> (N, c_x, c_y);
    *bytes = N*2*sizeof(float);
  } else if(!strcmp(op, "pow")){
    cudaPow <<< G, T >>> (N, c_x, c_y, c_z);
    *bytes = N*3*sizeof(float);
  }
  else if(!strcmp(op, "sin")){
    cudaSin <<< G, T >>> (N, c_x, c_y);
    *bytes = N*2*sizeof(float);
  }else if(!strcmp(op, "tan")){
    cudaTan <<< G, T >>> (N, c_x, c_y);
    *bytes = N*2*sizeof(float);
  }
  else if(!strcmp(op, "atan2")){
    cudaAtan2 <<< G, T >>> (N, c_x, c_y, c_z);
    *bytes = N*3*sizeof(float);
  }

}

double timeKernel(const char *op, int N, float *c_x, float *c_y, float *c_z){

  size_t bytes;

  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);
  cudaEventRecord(tic);

  launcher(op, N, c_x, c_y, c_z, &bytes);

  cudaEventRecord(toc);
  cudaDeviceSynchronize();
  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.;

  printf("%7.5e %7.5e ; %%%% kernel %s, time (s), throughput (GB/s)\n",
	 elapsed, (bytes/1.e9)/elapsed, op);
  
  return elapsed;
}

int main(int argc, char **argv){

  float *h_x, *h_y, *h_z;
  float *c_x, *c_y, *c_z;

  int N = atoi(argv[1]);

  h_x = (float*) calloc(N, sizeof(float));
  h_y = (float*) calloc(N, sizeof(float));
  h_z = (float*) calloc(N, sizeof(float));

  for(int n=0;n<N;++n){
    h_x[n] = .1 + drand48();
    h_y[n] = .1 + drand48();
    h_z[n] = .1 + drand48();
  }

  cudaMalloc(&c_x, N*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));
  cudaMalloc(&c_z, N*sizeof(float));

  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_y, h_y, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_z, h_z, N*sizeof(float), cudaMemcpyHostToDevice);
  
  timeKernel("fabs", N, c_x, c_y, c_z);
  timeKernel("log",  N, c_x, c_y, c_z);
  timeKernel("exp",  N, c_x, c_y, c_z);
  timeKernel("sqrt", N, c_x, c_y, c_z);
  timeKernel("pow",  N, c_x, c_y, c_z);
  timeKernel("sin",  N, c_x, c_y, c_z);
  timeKernel("tan",  N, c_x, c_y, c_z);
  timeKernel("atan2",  N, c_x, c_y, c_z);
  

}
