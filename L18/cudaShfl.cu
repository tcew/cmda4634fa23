


#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void shflKernel(int N, int *x, int *y){

  int n = threadIdx.x + blockIdx.x*blockDim.x;

  int xn = (n<N) ? x[n] : 0;

  // donor thread
  int srcLane = 0;

  // magic number (threads in a warp)
  int warpSize = 32;

  // mask to decide which threads are involved 
  unsigned int mask = 0xffffffff;
  
  int yn =  __shfl_sync(mask, xn, srcLane, warpSize);
  
  if(n<N){
    y[blockIdx.x] = yn;
  }

}


__global__ void shflReductionKernel(int N, int *x, int *y){

  int t = threadIdx.x;
  int w = threadIdx.y;
  int n = threadIdx.x + threadIdx.y*32 + blockIdx.x*1024;

  int xn = (n<N) ? x[n] : 0;

  // donor thread
  int srcLane = 0;

  // magic number (threads in a warp)
  int warpSize = 32;

  // mask to decide which threads are involved 
  unsigned int mask = 0xffffffff;

  int tmp;
  
  xn +=  __shfl_sync(mask, xn, t + 16, warpSize);
  xn +=  __shfl_sync(mask, xn, t +  8, warpSize);
  xn +=  __shfl_sync(mask, xn, t +  4, warpSize);
  xn +=  __shfl_sync(mask, xn, t +  2, warpSize);
  xn +=  __shfl_sync(mask, xn, t +  1, warpSize);

  __shared__ int s_x[32];
  if(t==0){
    s_x[w] = xn;
  }

  __syncthreads();
  
  if(w==0){
    xn = s_x[t];
    
    xn +=  __shfl_sync(mask, xn, t + 16, warpSize);
    xn +=  __shfl_sync(mask, xn, t +  8, warpSize);
    xn +=  __shfl_sync(mask, xn, t +  4, warpSize);
    xn +=  __shfl_sync(mask, xn, t +  2, warpSize);
    xn +=  __shfl_sync(mask, xn, t +  1, warpSize);
    
    if(n<N && t==0){
      y[blockIdx.x] = xn;
    }
  }

}


int main(int argc, char **argv){

  int N = atoi(argv[1]);

  int *h_x = (int*) calloc(N, sizeof(int));
  int *h_y = (int*) calloc(N, sizeof(int));

  for(int n=0;n<N;++n){
    h_x[n] = 1;
  }
  
  int *c_x, *c_y;
  cudaMalloc(&c_x, N*sizeof(int));
  cudaMalloc(&c_y, N*sizeof(int));

  cudaMemcpy(c_x, h_x, N*sizeof(int), cudaMemcpyHostToDevice);

  dim3 T(32,32);
  int G = (N+1024-1)/1024;

  //  shflKernel <<< G, T >>> (N, c_x, c_y);
  shflReductionKernel <<< G, T >>> (N, c_x, c_y);

  cudaMemcpy(h_y, c_y, N*sizeof(int), cudaMemcpyDeviceToHost);

  for(int n=0;n<N/1024;++n){
    printf("h_y[%d]=%d\n", n, h_y[n]);
  }

}
  
