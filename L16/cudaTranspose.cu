
#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>


__global__ void transposeKernelV00 (int N, float *X, float *XT){

  for(int r=0;r<N;++r){
    for(int c=0;c<N;++c){

      XT[r + c*N] = X[r*N+c];
      
    }
  }
  
}

void transposeLauncherV00 (int N, float *c_X, float *c_Y){

  dim3 G(1), B(1);

  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  transposeKernelV00 <<< G, B >>> (N, c_X, c_Y);
  cudaEventRecord(toc);

  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.f;

  double gbytes = sizeof(float)*N*N*2/1.e9;

  printf("Kernel 00: elapsed %g, throughput %g GB/s\n", elapsed, gbytes/elapsed);
}


__global__ void transposeKernelV01 (int N, float *X, float *XT){

  int r = threadIdx.x + blockDim.x*blockIdx.x;
  int c = threadIdx.y + blockDim.x*blockIdx.y;

  if(r<N && c<N)
    XT[r + c*N] = X[r*N+c];
  
}

void transposeLauncherV01 (int N, float *c_X, float *c_Y){

  int T = 32;
  
  dim3 G((N+T-1)/T, (N+T-1)/T), B(T,T);

  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  transposeKernelV01 <<< G, B >>> (N, c_X, c_Y);
  cudaEventRecord(toc);

  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.f;

  double gbytes = sizeof(float)*N*N*2/1.e9;

  printf("Kernel 01: elapsed %g, throughput %g GB/s\n", elapsed, gbytes/elapsed);

}


__global__ void transposeKernelV02 (int N, float *X, float *XT){

  int tr = threadIdx.x;
  int tc = threadIdx.y;

  int br = blockIdx.x;
  int bc = blockIdx.y;
  
  int r = threadIdx.x + 32*blockIdx.x;
  int c = threadIdx.y + 32*blockIdx.y;
  
  __shared__ float s_X[32][32];
  
  s_X[tc][tr] = (r<N && c<N) ? X[r + N*c]:0;

  __syncthreads();
  
  int rout = threadIdx.x + 32*bc;
  int cout = threadIdx.y + 32*br;
  
  if(rout<N && cout<N)
    XT[rout + cout*N] = s_X[tr][tc];
  
}

void transposeLauncherV02 (int N, float *c_X, float *c_Y){

  int T = 32;
  
  dim3 G((N+T-1)/T, (N+T-1)/T), B(T,T);

  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  transposeKernelV02 <<< G, B >>> (N, c_X, c_Y);
  cudaEventRecord(toc);

  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.f;

  double gbytes = sizeof(float)*N*N*2/1.e9;

  printf("Kernel 02: elapsed %g, throughput %g GB/s\n", elapsed, gbytes/elapsed);

}

#ifndef p_T
#define p_T 12 
#endif

__global__ void transposeKernelV03 (int N, float *X, float *XT){

  int tr = threadIdx.x;
  int tc = threadIdx.y;

  int br = blockIdx.x;
  int bc = blockIdx.y;
  
  int rin = tr + p_T*br;
  int cin = tc + p_T*bc;
  
  __shared__ float s_X[p_T][p_T+1];
  
  s_X[tc][tr] = (rin<N && cin<N) ? X[rin + N*cin]:0;

  __syncthreads();
  
  int rout = threadIdx.x + p_T*bc;
  int cout = threadIdx.y + p_T*br;
  
  if(rout<N && cout<N)
    XT[rout + cout*N] = s_X[tr][tc];
  
}

void transposeLauncherV03 (int N, float *c_X, float *c_Y){

  dim3 G((N+p_T-1)/p_T, (N+p_T-1)/p_T), B(p_T,p_T);

  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  // https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__TYPES.html#group__CUDART__TYPES_1gb980f35ed69ee7991704de29a13de49b
  // options for cache config:
  /*
    cudaFuncCachePreferNone = 0
    Default function cache configuration, no preference
    cudaFuncCachePreferShared = 1
    Prefer larger shared memory and smaller L1 cache
    cudaFuncCachePreferL1 = 2
    Prefer larger L1 cache and smaller shared memory
    cudaFuncCachePreferEqual = 3
    Prefer equal size L1 cache and shared memory
   */        
  cudaFuncSetCacheConfig(transposeKernelV03, cudaFuncCachePreferShared); 

  transposeKernelV03 <<< G, B >>> (N, c_X, c_Y);

  cudaEventRecord(tic);
  transposeKernelV03 <<< G, B >>> (N, c_X, c_Y);
  cudaEventRecord(toc);

  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.f;

  double gbytes = sizeof(float)*N*N*2/1.e9;

  printf("%d, %g; %%%% Occupancy test Kernel 03: elapsed %g, throughput %g GB/s, block size %d\n",
    	      p_T, gbytes/elapsed, elapsed, gbytes/elapsed, p_T);

}

#ifndef p_TY
#define p_TY 4
#endif

// use a rectangle array of threads
// - loads p_T x p_T entries, in chunks
__global__ void transposeKernelV04 (int N, float *X, float *XT){

  int tr = threadIdx.x;
  int tc = threadIdx.y;

  int br = blockIdx.x;
  int bc = blockIdx.y;
  
  __shared__ float s_X[p_T][p_T+1];

  int tcin = tc;
  int rin = tr + p_T*br;
  while(tcin<p_T){
    int cin = tcin + p_T*bc;
    s_X[tcin][tr] = (rin<N && cin<N) ? X[rin + N*cin]:0;
    tcin += p_TY;
  }
      
  __syncthreads();
  
  int rout = threadIdx.x + p_T*bc;
  int tcout= tc;
  while(tcout<p_T){
    int cout = tcout + p_T*br;
    if(rout<N && cout<N){
      XT[rout + N*cout] = s_X[tr][tcout];
    }
    tcout += p_TY;
  }
  
}

void transposeLauncherV04 (int N, float *c_X, float *c_Y){
  
  dim3 G((N+p_T-1)/p_T, (N+p_T-1)/p_T), B(p_T,p_TY);

  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  // https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__TYPES.html#group__CUDART__TYPES_1gb980f35ed69ee7991704de29a13de49b
  // options for cache config:
  /*
    cudaFuncCachePreferNone = 0
    Default function cache configuration, no preference
    cudaFuncCachePreferShared = 1
    Prefer larger shared memory and smaller L1 cache
    cudaFuncCachePreferL1 = 2
    Prefer larger L1 cache and smaller shared memory
    cudaFuncCachePreferEqual = 3
    Prefer equal size L1 cache and shared memory
  */        
  cudaFuncSetCacheConfig(transposeKernelV04, cudaFuncCachePreferShared); 

  transposeKernelV04 <<< G, B >>> (N, c_X, c_Y);

  cudaEventRecord(tic);
  transposeKernelV04 <<< G, B >>> (N, c_X, c_Y);
  cudaEventRecord(toc);

  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.f;

  double gbytes = sizeof(float)*N*N*2/1.e9;

  printf("Kernel 04: elapsed %g, throughput %g GB/s, block size (%d,%d)\n",
	 elapsed, gbytes/elapsed, p_T, p_TY);

}

int main(int argc, char **argv){

  int N = atoi(argv[1]);

  float *c_X, *c_Y;

  cudaMalloc(&c_X, N*N*sizeof(float));
  cudaMalloc(&c_Y, N*N*sizeof(float));

//  transposeLauncherV01(N, c_X, c_Y);
//  transposeLauncherV02(N, c_X, c_Y);
  transposeLauncherV03(N, c_X, c_Y);
//  transposeLauncherV04(N, c_X, c_Y);
}
