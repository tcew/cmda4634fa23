/*

To compile:
gcc -O3  -o mandelbrot mandelbrot.c png_util.c -I. -lm -lpng

To run:
./mandelbrot

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "png_util.h"

void mandelbrot(const int Nx, 
		const int Ny, 
		const float minx,
		const float miny,
		const float hx, 
		const float hy,
		float * __restrict__ h_count){

  int n,m;
  
  for(m=0;m<Ny;++m){
    for(n=0;n<Nx;++n){
      
      float cx = minx + n*hx;
      float cy = miny + m*hy;

      float zx = 0;
      float zy = 0;
      
      int Nt = 200;
      int t, cnt=0;
      for(t=0;t<Nt;++t){
	
	// z = z^2 + c
	//   = (zx + i*zy)*(zx + i*zy) + (cx + i*cy)
	//   = zx^2 - zy^2 + 2*i*zy*zx + cx + i*cy
	float zxTmp = zx*zx - zy*zy + cx;
	zy = 2.f*zy*zx + cy;
	zx = zxTmp;

	cnt += (zx*zx+zy*zy<4.f);
      }

      h_count[n + m*Nx] = cnt;
    }
  }
  

}


int main(int argc, char **argv){

  const int Nx = 4096;
  const int Ny = 4096;

  /* box containing sample points */
  const float centx = -.759856, centy= .125547;
  const float diam  = 0.151579;
  const float minx = centx-0.5*diam;
  const float remax = centx+0.5*diam;
  const float miny = centy-0.5*diam;
  const float immax = centy+0.5*diam;

  const float dx = (remax-minx)/(Nx-1.f);
  const float dy = (immax-miny)/(Ny-1.f);

  float *h_count = (float*) calloc(Nx*Ny, sizeof(float));

  double tic = clock();

  // call mandelbrot from here
  mandelbrot(Nx, Ny, minx, miny, dx, dy, h_count);

  double toc = clock();

  double elapsed = (toc-tic)/CLOCKS_PER_SEC;
  
  printf("elapsed time %g\n", elapsed);

  FILE *png = fopen("mandelbrot.png", "w");
  write_hot_png(png, Nx, Ny, h_count, 0, 80);
  fclose(png);

}
