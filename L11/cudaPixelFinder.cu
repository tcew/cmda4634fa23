
#include <cuda.h>
#include <stdio.h>


__global__ void cudaPixelFinderKernel(int Nx, int Ny, float *dataIn, float *dataOut  ){

  int tx = threadIdx.x; // x-coordinate in thread block
  int ty = threadIdx.y; // y-coordinate in thread block

  int bx = blockIdx.x; // x-coordinate of block
  int by = blockIdx.y; // y-coordinate of block

  int dx = blockDim.x; // x-dimension of thread-block
  int dy = blockDim.y; // y-dimension of thread-block

  int nx = dx*bx + tx; // global x index of thread
  int ny = dy*by + ty; // global y index of thread

  // assume dataIn and dataOut are row-major
  int n = nx + ny*Nx;

  // now load data
  if(nx<Nx && ny<Ny){
    float foo = dataIn[n];
    dataOut[n] = foo*foo;
  }
  
}


int main(int argc, char **argv){


  int Nx = 1000;
  int Ny = 1000;

  float *c_dataIn, *c_dataOut;
  cudaMalloc(&c_dataIn, Nx*Ny*sizeof(float));
  cudaMalloc(&c_dataOut, Nx*Ny*sizeof(float));
  
  int D = 16;
  dim3 B(D,D);
  dim3 G( (Nx+D-1)/D, (Ny+D-1)/D);
  
  cudaPixelFinderKernel <<< G, B >>> (N, c_dataIn, c_dataOut);

}
