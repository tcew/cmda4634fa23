
#include <stdio.h>
#include <time.h>

// kernel to copy data from array x to array y
__global__
void buggyKernel1(int N, float *x, float *y){
  
  int t  = threadIdx.x; // in the range 0 to number of threads per thread-block
  int b  = blockIdx.x;  // in the range 0 to number of blocks-1
  int d  = blockDim.x;  // how many threads in each thread-block

  int n = b*d + t;

  y[n+1] = x[n]; 
  
}


int main(int argc, char **argv){

  if(argc<2){
    printf("usage: ./cudaBugs N\n");
    exit(-1);
  }

  int N = atoi(argv[1]);

  float *h_x, *h_y;
  float *c_x, *c_y;

  // allocate arrays on HOST
  h_x = (float*) malloc(N*sizeof(float));
  h_y = (float*) malloc(N*sizeof(float));
  
  for(int n=0;n<10;++n){
    h_x[n] = n;
  }
  
  // allocate array on DEVICE
  cudaMalloc(&c_x, N*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));

  // THE FOLLOWING TEST CASES INCLUDE COMMON ERRORS MADE WHEN CODING CUDA
  // TROUBLESHOOT EACH OF THE FOLLOWING SELF-CONTAINED TEST CASES:
  // 1. identify the problems for each test
  // 2. add comments for mistakes found
  // 3. comment out the mistake with // and fix the mistakes
  // 4. you should assume all comments indicate actual intent of the code
  
  {    
    // TEST 1: copy data from HOST to DEVICE array, then to a different DEVICE array, then back to HOST

    int T = 256;       // threads per thread-block
    int G = (N+T-1)/T; // thread-blocks
    
    // copy data from HOST to DEVICE
    cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyHostToDevice);

    // call kernel1
    buggyKernel1 <<< G , T  >>> (N, c_x, c_y);

    // copy data from DEVICE to HOST
    cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  }


  {    
    // TEST 2: copy data from HOST to DEVICE array, then to a different DEVICE array, then back to HOST

    int T = 256;       // threads per thread-block
    int G = (N+T-1)/T; // thread-blocks
    
    // copy data from HOST to DEVICE
    cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyDeviceToHost);

    // call kernel1
    buggyKernel1 <<< G , T  >>> (N, c_x, c_y);

    // copy data from DEVICE to HOST
    cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  }


  {    
    // TEST 3: copy data from HOST to DEVICE array, then to a different DEVICE array, then back to HOST
    
    int T = 256;       // threads per thread-block
    int G = (N+T-1)/T; // thread-blocks
    
    // copy data from HOST to DEVICE
    cudaMemcpy(c_x, h_x, N, cudaMemcpyHostToDevice);

    // call kernel1
    buggyKernel1 <<< T, G  >>> (N, c_x, c_y);

    // copy data from DEVICE to HOST
    cudaMemcpy(h_x, c_x, N, cudaMemcpyDeviceToHost);
  }


  {    
    // TEST 4: copy data from HOST to DEVICE array, then to a different DEVICE array, then back to HOST

    int T = 256;       // threads per thread-block
    int G = (N+T-1)/T; // thread-blocks
    
    // copy data from HOST to DEVICE
    cudaMemcpy(c_x, h_x, N, cudaMemcpyHostToDevice);

    // call kernel1
    buggyKernel1 <<< T, G  >>> (N, c_x, c_y);

    // copy data from DEVICE to HOST
    cudaMemcpy(h_x, c_x, N, cudaMemcpyDeviceToHost);
  }

  {
    // TEST 5: time data copy  kernel
    int T = 256;       // threads per thread-block
    int G = (N+T-1)/T; // thread-blocks

    // use events to measure the elapsed time for the kernel
    cudaEvent_t start, stop;
    
    cudaEventRecord(start);
    buggyKernel1 <<< G , T  >>> (N, c_x, c_y);
    cudaEventRecord(stop);

    float deviceElapsed;
    cudaEventElapsedTime(&deviceElapsed, start, stop);
    
    printf("deviceElapsed = %e seconds\n",  deviceElapsed);
  }


  {
    // TEST 6: copy data from HOST to DEVICE array, then to a different DEVICE array, then back to HOST
    int T = 2560;       // threads per thread-block
    int G = (N+T-1)/T; // thread-blocsk
    
    // copy data from HOST to DEVICE
    cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyHostToDevice);

    // call kernel1
    buggyKernel1 <<< G , T  >>> (N, h_x, h_y);

    // copy data from DEVICE to HOST
    cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);

  }
  
  

#if 0
    
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaDeviceSynchronize();


  double gigabytes = N*2*sizeof(float)/1.e9;

  double achievedBandwidth = gigabytes/deviceElapsed; 

  printf("%e,%e, %e %%%% gigabytes, achievedBandwidth (GB/s)\n",
  	 gigabytes, deviceElapsed, achievedBandwidth);
  
  // copy data from DEVICE to HOST
  cudaMemcpy(h_x, c_y, N*sizeof(float), cudaMemcpyDeviceToHost);

#endif
  
  cudaFree(c_x);
  cudaFree(c_y);
  free(h_x);
  
  cudaFree(c_y);
  
}
