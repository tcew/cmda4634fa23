
#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>

#define p_T 1024

#define dfloat float

// v0: horrible serial code
__global__ void reductionKernelV00(int N, dfloat *x, dfloat *sumx){

  // sum up N values from x
  dfloat sum = 0;
  for(int n=0;n<N;++n){
    sum += x[n];
  }

  sumx[0] = sum;

}

// v1: half a vector  (run this log2(N) times)
__global__ void reductionKernelV01(int N, int M, dfloat *x, dfloat *sumx){

  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N && n<M && n+M<N){
    sumx[n] = x[n] + x[n+M];
  }
  
}

// v2: partial reduction by one thread using parallel loaded data
__global__ void reductionKernelV02(int N, dfloat *x, dfloat *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ dfloat s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t==0){
    dfloat sum = 0;
    for(int m=0;m<p_T;++m){
      sum += s_x[m];
    }
    sumx[blockIdx.x] = sum;
  }
  
}


// v3: partial reduction by one thread using parallel loaded data
__global__ void reductionKernelV03(int N, dfloat *x, dfloat *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ dfloat s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t==0){
    dfloat sum = 0;
    for(int m=0;m<p_T;++m){
      sum += s_x[m];
    }
    sumx[blockIdx.x] = sum;
  }
  
}

// v4: partial reduction by one thread using parallel loaded data
__global__ void reductionKernelV04(int N, dfloat *x, dfloat *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ dfloat s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t<512) s_x[t] += s_x[t+512];

  __syncthreads();

  if(t<256) s_x[t] += s_x[t+256];
  
  __syncthreads();

  if(t<128) s_x[t] += s_x[t+128];

  __syncthreads();

  if(t<64) s_x[t] += s_x[t+64];

  __syncthreads();

  if(t<32) s_x[t] += s_x[t+32];

  __syncthreads();

  if(t<16) s_x[t] += s_x[t+16];

  __syncthreads();
  
  if(t<8) s_x[t] += s_x[t+8];

  __syncthreads();

  if(t<4) s_x[t] += s_x[t+4];

  __syncthreads();

  if(t<2) s_x[t] += s_x[t+2];

  __syncthreads();

  if(t<1){
    s_x[t] += s_x[t+1];
    sumx[blockIdx.x] = s_x[0];
  }
}


// v5: throw away some synchronizations
__global__ void reductionKernelV05(int N, dfloat *x, dfloat *sumx){

  // guaranteed cache space on the SM running this thread-block
  volatile __shared__ dfloat s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t<512) s_x[t] += s_x[t+512];

  __syncthreads();

  if(t<256) s_x[t] += s_x[t+256];
  
  __syncthreads();

  if(t<128) s_x[t] += s_x[t+128];

  __syncthreads();

  if(t<64) s_x[t] += s_x[t+64];

  __syncthreads();

  if(t<32) s_x[t] += s_x[t+32];

  if(t<16) s_x[t] += s_x[t+16];
  
  if(t<8) s_x[t] += s_x[t+8];

  if(t<4) s_x[t] += s_x[t+4];

  if(t<2) s_x[t] += s_x[t+2];

  if(t<1){
    s_x[t] += s_x[t+1];
    sumx[blockIdx.x] = s_x[0];
  }
}



// v6: only use warp sync and one thread-block sync
__global__ void reductionKernelV06(int N, dfloat *x, dfloat *sumx){

  // guaranteed cache space on the SM running this thread-block
  volatile __shared__ dfloat s_x[32][32];
  volatile __shared__ dfloat s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = threadIdx.x + 32*w + blockIdx.x*1024;

  if(n<N){
    s_x[w][t] = x[n];
  }

  // barrier all threads in this thread-block
  if(t<16) s_x[w][t] += s_x[w][t+16];
  if(t< 8) s_x[w][t] += s_x[w][t+8];
  if(t< 4) s_x[w][t] += s_x[w][t+4];
  if(t< 2) s_x[w][t] += s_x[w][t+2];
  if(t< 1) s_x[w][t] += s_x[w][t+1];

  if(t==0){
    s_sumx[t] = s_x[w][0];
  }
  
  __syncthreads();

  if(w==0){
    if(t<16) s_sumx[t] += s_sumx[t+16];
    if(t<8)  s_sumx[t] += s_sumx[t+8];
    if(t<4)  s_sumx[t] += s_sumx[t+4];
    if(t<2)  s_sumx[t] += s_sumx[t+2];
    if(t<1)  s_sumx[t] += s_sumx[t+1];

    if(t==0){
      sumx[blockIdx.x] = s_sumx[0];
    }
  }
}


// v7: use warpsync
__global__ void reductionKernelV07(int N, dfloat *x, dfloat *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ dfloat s_x[32][32];
  __shared__ dfloat s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = t + 32*w + blockIdx.x*1024;

  if(n<N){
    s_x[w][t] = x[n];
  }

  // barrier all threads in this thread-block
  if(t<16) s_x[w][t] += s_x[w][t+16];
  __syncwarp();
  if(t< 8) s_x[w][t] += s_x[w][t+8];
  __syncwarp();
  if(t< 4) s_x[w][t] += s_x[w][t+4];
  __syncwarp();
  if(t< 2) s_x[w][t] += s_x[w][t+2];
  __syncwarp();
  if(t< 1) s_x[w][t] += s_x[w][t+1];
  __syncwarp();

  if(t==0){
    s_sumx[w] = s_x[w][0];
  }
  
  __syncthreads();

  if(w==0){
    if(t<16) s_sumx[t] += s_sumx[t+16];
    __syncwarp();
    if(t<8)  s_sumx[t] += s_sumx[t+8];
    __syncwarp();
    if(t<4)  s_sumx[t] += s_sumx[t+4];
    __syncwarp();    
    if(t<2)  s_sumx[t] += s_sumx[t+2];
    __syncwarp();
    if(t<1)  s_sumx[t] += s_sumx[t+1];

    if(t==0){
      sumx[blockIdx.x] = s_sumx[0];
    }
  }
}


// v8: pipeline data loads at start
__global__ void reductionKernelV08(int N, dfloat *x, dfloat *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ dfloat s_x[32][32];
  __shared__ dfloat s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = t + 32*w + blockIdx.x*1024;

  dfloat tmp = 0;

  int m = n;
  while(m<N){
    tmp += x[m];
    m += blockDim.x*blockDim.y*gridDim.x;
  }
  s_x[w][t] = tmp;
  __syncwarp();
  
  // barrier all threads in this thread-block
  if(t<16) s_x[w][t] += s_x[w][t+16];
  __syncwarp();
  if(t< 8) s_x[w][t] += s_x[w][t+8];
  __syncwarp();
  if(t< 4) s_x[w][t] += s_x[w][t+4];
  __syncwarp();
  if(t< 2) s_x[w][t] += s_x[w][t+2];
  __syncwarp();
  if(t< 1) s_x[w][t] += s_x[w][t+1];
  __syncwarp();

  if(t==0){
    s_sumx[w] = s_x[w][0];
  }
  
  __syncthreads();

  if(w==0){
    if(t<16) s_sumx[t] += s_sumx[t+16];
    __syncwarp();
    if(t<8)  s_sumx[t] += s_sumx[t+8];
    __syncwarp();
    if(t<4)  s_sumx[t] += s_sumx[t+4];
    __syncwarp();    
    if(t<2)  s_sumx[t] += s_sumx[t+2];
    __syncwarp();
    if(t<1)  s_sumx[t] += s_sumx[t+1];

    if(t==0){
      sumx[blockIdx.x] = s_sumx[0];
    }
  }
}

// atomic
__global__ void reductionKernelV09(int N, dfloat *x, dfloat *sumx){

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = t + 32*w + blockIdx.x*1024;

  if(n<N){
    //    s_x[t] = x[n];
    int m = n;
    dfloat tmp = 0;
    while(m<N){
      tmp += x[m];
      m += blockDim.x*blockDim.y*gridDim.x;
    }
    atomicAdd(sumx, tmp);
  }
}

#define p_T10 128

// v10: mutiple loads 
__global__ void reductionKernelV10(int N, dfloat *x, dfloat *sumx){
  
  int n = threadIdx.x + blockIdx.x*p_T10;
  
  int m = n;
  dfloat tmp = 0;
  while(m<N){
    tmp += x[m];
    m += p_T10*gridDim.x;
  }
  sumx[n] = tmp;
}


#define p_T11 128

// v11: changed memory blocking
__global__ void reductionKernelV11(int N, int M, int Nreads, dfloat *x, dfloat *sumx){
  
  int n = threadIdx.x + blockIdx.x*(p_T11*Nreads);
  int m = n;
  dfloat tmp = 0;

#pragma unroll 16
  for(int r=0;r<Nreads;++r){
    if(m<N){
      tmp += x[m];
    }
    m += p_T11;
  }

  n = threadIdx.x + blockIdx.x*p_T11;
  if(n<M)
    sumx[n] = tmp;
}

// v12: pipeline  + shfl
__global__ void reductionKernelV12(int N, dfloat *x, dfloat *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ dfloat s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = t + 32*w + blockIdx.x*1024;

  dfloat r_x = 0;

  int m = n;
  while(m<N){
    r_x += x[m];
    m += blockDim.x*blockDim.y*gridDim.x;
  }
  
  // barrier all threads in this thread-block
#define MASK 0xffffffff
  
  r_x += __shfl_down_sync(MASK, r_x, 16);
  r_x += __shfl_down_sync(MASK, r_x,  8);
  r_x += __shfl_down_sync(MASK, r_x,  4);
  r_x += __shfl_down_sync(MASK, r_x,  2);
  r_x += __shfl_down_sync(MASK, r_x,  1);

  if(t==0){
    s_sumx[w] = r_x;
  }
  
  __syncthreads();

  if(w==0){
    r_x = s_sumx[t];
    r_x += __shfl_down_sync(MASK, r_x, 16);
    r_x += __shfl_down_sync(MASK, r_x,  8);
    r_x += __shfl_down_sync(MASK, r_x,  4);
    r_x += __shfl_down_sync(MASK, r_x,  2);
    r_x += __shfl_down_sync(MASK, r_x,  1);
    
    if(t==0){
      sumx[blockIdx.x] = r_x;
    }
  }
}

// v12: pipeline  + shfl + blocking
__global__ void reductionKernelV13(int N, int Nreads, dfloat *x, dfloat *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ dfloat s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = t + 32*w + blockIdx.x*1024*Nreads;

  dfloat r_x = 0;

  int m = n;
  for(int r=0;r<Nreads;++r){
    if(m<N){
      r_x += x[m];
    }
    m += blockDim.x*blockDim.y;
  }
  
  // barrier all threads in this thread-block
#define MASK 0xffffffff
  
  r_x += __shfl_down_sync(MASK, r_x, 16);
  r_x += __shfl_down_sync(MASK, r_x,  8);
  r_x += __shfl_down_sync(MASK, r_x,  4);
  r_x += __shfl_down_sync(MASK, r_x,  2);
  r_x += __shfl_down_sync(MASK, r_x,  1);

  if(t==0){
    s_sumx[w] = r_x;
  }
  
  __syncthreads();

  if(w==0){
    r_x = s_sumx[t];
    r_x += __shfl_down_sync(MASK, r_x, 16);
    r_x += __shfl_down_sync(MASK, r_x,  8);
    r_x += __shfl_down_sync(MASK, r_x,  4);
    r_x += __shfl_down_sync(MASK, r_x,  2);
    r_x += __shfl_down_sync(MASK, r_x,  1);
    
    if(t==0){
      sumx[blockIdx.x] = r_x;
    }
  }
}




void reductionLauncher(int knl, int N, dfloat *h_x, dfloat *c_x, dfloat *c_sumx, dfloat *h_sumx){

  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaMemcpy(c_x, h_x, N*sizeof(dfloat), cudaMemcpyHostToDevice);
  
  cudaEventRecord(tic);

  for(int test=0;test<1;++test)
  switch(knl){
  case 0:{
    // one thread
    dim3 G(1), B(1); 
    reductionKernelV00 <<< G, B >>> (N, c_x, c_sumx);
    break;
  }
  case 1:{

    int M = N;
    int cnt = 0;
    
    while(M>p_T){    
      int G = (M+p_T-1)/p_T;
      
      if(G>0){
	if(cnt%2==0)
	  reductionKernelV01 <<< G, p_T >>> (N, M, c_x, c_sumx);
	else
	  reductionKernelV01 <<< G, p_T >>> (N, M, c_sumx, c_x);
      }
      M = (M+1)/2;
      ++cnt;
    }

    if(cnt%2==0){ // result is in c_x;
      cudaMemcpy(h_sumx, c_sumx, M*sizeof(dfloat), cudaMemcpyDeviceToHost);
    }
    else{ // result in c_sumx
      cudaMemcpy(h_sumx, c_x, M*sizeof(dfloat), cudaMemcpyDeviceToHost);
    }

    break;
  }
  case 2:{
    // one thread
    dim3 G((N+p_T-1)/p_T), B(p_T); 
    reductionKernelV02 <<< G, B >>> (N, c_x, c_sumx);

    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(dfloat), cudaMemcpyDeviceToHost);
    dfloat sum = 0;
    for(int i=0;i<G.x;++i){
      sum += h_sumx[i];
    }
	   
    break;
  }
  case 3:{
    // one thread
    dim3 G((N+p_T-1)/p_T), B(p_T); 
    reductionKernelV03 <<< G, B >>> (N, c_x, c_sumx);

    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(dfloat), cudaMemcpyDeviceToHost);
    dfloat sum = 0;
    for(int i=0;i<G.x;++i){
      sum += h_sumx[i];
    }
    
    break;
  }
  case 4:{
    // one thread
    dim3 G((N+p_T-1)/p_T), B(p_T); 
    reductionKernelV04 <<< G, B >>> (N, c_x, c_sumx);

    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(dfloat), cudaMemcpyDeviceToHost);
    dfloat sum = 0;
    for(int i=0;i<G.x;++i){
      sum += h_sumx[i];
    }
    
    break;
  }
  case 5:{
    // one thread
    dim3 G((N+p_T-1)/p_T), B(p_T); 
    reductionKernelV05 <<< G, B >>> (N, c_x, c_sumx);

    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(dfloat), cudaMemcpyDeviceToHost);
    dfloat sum = 0;
    for(int i=0;i<G.x;++i){
      sum += h_sumx[i];
    }
    
    break;
  }
  case 6:{
    dim3 G((N+1024-1)/1024,1);
    dim3 B(32,32); 

    reductionKernelV06 <<< G, B >>> (N, c_x, c_sumx);

    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(dfloat), cudaMemcpyDeviceToHost);
    dfloat sum = 0;
    for(int i=0;i<G.x;++i){
      sum += h_sumx[i];
    }
    
    break;
  }
  case 7:{
    dim3 G((N+1024-1)/1024), B(32,32); 
    reductionKernelV07 <<< G, B >>> (N, c_x, c_sumx);

    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(dfloat), cudaMemcpyDeviceToHost);
    dfloat sum = 0;
    for(int i=0;i<G.x;++i){
      sum += h_sumx[i];
    }
    
    break;
  }
  case 8:{
    int M = (N+15)/16;
    //    int M = (N+31)/32;
    dim3 G((M+1024-1)/1024,1), B(32,32); 
    reductionKernelV08 <<< G, B >>> (N, c_x, c_sumx);

#if 0
    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(dfloat), cudaMemcpyDeviceToHost);

    for(int n=1;n<G.x;++n){
      h_sumx[0]+=h_sumx[n];
    }
    //    printf("V08: h_sumx[0]=%g, %d outputs\n", h_sumx[0], G.x);

#endif
    
    break;
  }
  case 9:{
    int M = (N+15)/16;
    dim3 G((M+1024-1)/1024), B(32,32);
    // zero out accumulator
    cudaMemset(c_sumx, 0, sizeof(dfloat));
    reductionKernelV09 <<< G, B >>> (N, c_x, c_sumx);

    cudaMemcpy(h_sumx, c_sumx, sizeof(dfloat), cudaMemcpyDeviceToHost);
    
    //    printf("G.x = %d,  h_sumx[0]=%f, one output\n", G.x, h_sumx[0]);
    
    break;
  }
  case 10:{

    int M = N;
    int cnt = 0;

    int reductionFactor = 16;
    
    while(M>p_T*reductionFactor){

      int Mold = M;

      // size of reduced output 
      M = (M+reductionFactor -1)/reductionFactor;
      
      int G = (M+p_T10-1)/p_T10;

      // correct number of outputs
      M = G*p_T10;
      
      if(G>0){
	if(cnt%2==0)
	  reductionKernelV10 <<< G, p_T10 >>> (Mold, c_x, c_sumx);
	else
	  reductionKernelV10 <<< G, p_T10 >>> (Mold, c_sumx, c_x);
	++cnt;
      }
    }

#if 0
    if(cnt%2==1){ // result is in c_x;
      cudaMemcpy(h_sumx, c_sumx, M*sizeof(dfloat), cudaMemcpyDeviceToHost);
    }
    else{ // result in c_sumx
      cudaMemcpy(h_sumx, c_x, M*sizeof(dfloat), cudaMemcpyDeviceToHost);
    }

    dfloat sum = 0;
    for(int n=0;n<M;++n){
      sum += h_sumx[n];
    }
#endif
    //    printf("V10: total sum %f, with %d outputs\n", sum, M);
    
    break;
  }

  case 11:{

    int M = N;
    int cnt = 0;

    int reductionFactor = 64;
    
    while(M>2048){

      int Mold = M;

      // size of reduced output 
      M = (M+reductionFactor-1)/reductionFactor;
      
      int G = (M+p_T11-1)/p_T11;

      // correct number of outputs
      M = G*p_T11;

      int Nreads = (Mold+M-1)/M;

      if(G>0){
	if(cnt%2==0)
	  reductionKernelV11 <<< G, p_T11 >>> (Mold, M, Nreads, c_x, c_sumx);
	else
	  reductionKernelV11 <<< G, p_T11 >>> (Mold, M, Nreads, c_sumx, c_x);
	++cnt;
      }
    }

    if(cnt%2==1){ // result is in c_x;
      cudaMemcpy(h_sumx, c_sumx, M*sizeof(dfloat), cudaMemcpyDeviceToHost);
    }
    else{ // result in c_sumx
      cudaMemcpy(h_sumx, c_x, M*sizeof(dfloat), cudaMemcpyDeviceToHost);
    }
    
    dfloat sum = 0;
    for(int n=0;n<M;++n){
      //      printf("h_sumx[%d]=%f\n", n, h_sumx[n]);
      sum += h_sumx[n];
    }
    //    printf("V11: total sum %f, with %d outputs\n", sum, M);

    break;
  }
  case 12:{
    int M = (N+15)/16;

    dim3 G((M+1024-1)/1024,1), B(32,32); 
    reductionKernelV12 <<< G, B >>> (N, c_x, c_sumx);

#if 0
    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(dfloat), cudaMemcpyDeviceToHost);

    for(int n=1;n<G.x;++n){
      h_sumx[0]+=h_sumx[n];
    }
    //    printf("V12: h_sumx[0]=%f, %d outputs\n", h_sumx[0], G.x);

#endif
    
    break;
  }
  case 13:{
    int Nreads = 16;
    int M = (N+Nreads-1)/Nreads;

    dim3 G((M+1024-1)/1024,1), B(32,32); 
    reductionKernelV13 <<< G, B >>> (N, Nreads, c_x, c_sumx);

#if 0
    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(dfloat), cudaMemcpyDeviceToHost);

    for(int n=1;n<G.x;++n){
      h_sumx[0]+=h_sumx[n];
    }
    //    printf("V12: h_sumx[0]=%f, %d outputs\n", h_sumx[0], G.x);

#endif
    
    break;
  }
  }

  cudaEventRecord(toc);

  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.f;
  
  printf("kernel %02d took %e s ", knl, elapsed);

  if(knl==0){
    dfloat h_sumx[1] = {0};
    cudaMemcpy(h_sumx, c_sumx, 1*sizeof(dfloat), cudaMemcpyDeviceToHost);
  }else{
    printf("\n");
  }
}

int main(int argc, char **argv){

  int N = atoi(argv[1]);
  
  dfloat *h_x    = (dfloat*) calloc(N, sizeof(dfloat));
  dfloat *h_sumx = (dfloat*) calloc(N, sizeof(dfloat));

  for(int n=0;n<N;++n){
    h_x[n] = 1;
  }
  
  dfloat *c_x, *c_sumx;
  cudaMalloc(&c_x,    N*sizeof(dfloat));
  cudaMalloc(&c_sumx, N*sizeof(dfloat));

  int Nknl = 14;
  int minKnl = 8;
  for(int knl=minKnl;knl<Nknl;++knl){
    reductionLauncher(knl, N, h_x, c_x, c_sumx, h_sumx);
  }
  
  for(int knl=minKnl;knl<Nknl;++knl){
    reductionLauncher(knl, N, h_x, c_x, c_sumx, h_sumx);
  }


  
  
}
