
#include <stdio.h>

__global__
void devicePrint(){
  
  int t  = threadIdx.x; // in the range 0 to number of threads per thread-block
  int b  = blockIdx.x;  // in the range 0 to number of blocks-1
  int d  = blockDim.x;  // how many threads in each thread-block
  int g  = gridDim.x;   // how many thread-blocks there are

  //  unique map between thread index, block index, and block size and integers
  int n = b*d + t;

  if(n==999)
    printf("Hello world from thread %d/%d in thread-block %d/%d ===> %d index!!!\n",
	 t, d, b, g, n);

  
}


void hostPrint(){

  printf("Hello world from CPU!!!\n");
  
}

int main(int argc, char **argv){

  int B = atoi(argv[1]);
  int T = atoi(argv[2]);
  
  hostPrint();
  
  devicePrint <<< B , T  >>> ();

  cudaDeviceSynchronize();
}
