#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void fmaKernel(int N, float *a, float *x, float *y){

  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    float an = a[n];
    float xn = x[n];
    float yn = y[n];
    float zn = an*xn + yn; // => translated into an fma
    y[n] = zn;
  }

}


int main(int argc, char **argv){

  int N = atoi(argv[1]);

  float *c_a, *c_x, *c_y;

  cudaMalloc(&c_a, N*sizeof(float));
  cudaMalloc(&c_x, N*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));

  int T = 256;
  int G = (N+T-1)/T;

  fmaKernel  <<< G, T >>> (N, c_a, c_x, c_y);

  cudaDeviceSynchronize();
  
}
