
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void logisticMapKernel(int N, int Niter, float *x, float rmin, float rmax){

  int t = threadIdx.x + blockIdx.x*blockDim.x;

  float r = rmin + (rmax-rmin)*t/(float)N;

  float xn = 0.5f;

  for(int n=0;n<Niter;++n){
    xn = r*xn*(1.f-xn);
  }

  if(t<N)
    x[t] = xn;
}


int main(int argc, char **argv){

  int N = atoi(argv[1]);
  int Niter = atoi(argv[2]);

  float rmin = 0.01;
  float rmax = 4;

  float *c_x, *h_x;

  cudaMalloc(&c_x, N*sizeof(float));
  
  int T = 256;
  int G = (N+T-1)/T;
  
  logisticMapKernel <<< G, T >>> (N, Niter, c_x, rmin, rmax);

  h_x = (float*) calloc(N, sizeof(float));
  cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyDeviceToHost);
}
