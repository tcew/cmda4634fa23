% https://en.wikipedia.org/wiki/CUDA

%% SM 89
NthreadsPerWarp = 32;
NsharedPerSM = 102.4*1024; % preferred shared partition
maxResidentWarps = 48;
maxResidentThreads = 1536;
maxResidentThreadBlocks = 24;

for T=1:32
    %% number of threads requested per thread-block
    NthreadsPerThreadBlock = T*T;

    %% number of warps (must divide by 32 but round up) [ limited by max number of thread-blocks per SM ]
    NwarpsPerThreadBlock = min(maxResidentWarps,ceil(NthreadsPerThreadBlock/NthreadsPerWarp));

    %% actual number of threads active (including idled threads)
    roundedNthreadsPerThreadBlock = NwarpsPerThreadBlock*NthreadsPerWarp;
    
    %% bytes in shared memory
    NsharedPerThreadBlock = 4*T*(T+1);

    %% shared memory limit
    maxThreadBlocks(T) = ...
        floor(NsharedPerSM/NsharedPerThreadBlock);

    %% max resident threads per SM limit
    maxThreadBlocks(T) = ...
        min(floor(maxResidentThreads/roundedNthreadsPerThreadBlock), maxThreadBlocks(T));

    %% max resident thread-blocks limit
    maxThreadBlocks(T) = ...
        min(maxResidentThreadBlocks,maxThreadBlocks(T));

    %% factor determining how many threads are actually active
    efficiency = NthreadsPerThreadBlock/roundedNthreadsPerThreadBlock;
    
    %% maximum effective occupancy percentage
    occupancy(T) = ...
        100*efficiency*min(1,(maxThreadBlocks(T)*NwarpsPerThreadBlock)/maxResidentWarps);
end
OD = load('OccupancyDataRTX4090.dat');
subplot(1,2,1); plot(occupancy, 'r-*', 'linewidth', 2)

grid on
xlabel('T')
ylabel('Effective occupancy')
set(gca, 'fontsize', 20)
set(gcf, 'color', 'w')

subplot(1,2,2); plot(OD(:,1), OD(:,2), 'b-s', 'linewidth', 2);

grid on
xlabel('T')
ylabel('Achieved throughput GB/s')
set(gca, 'fontsize', 20)
set(gcf, 'color', 'w')
