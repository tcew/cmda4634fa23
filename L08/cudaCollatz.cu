
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <time.h>
#include <omp.h>

#define min(a,b) ( (a)<(b) ? (a):(b))
#define max(a,b) ( (a)>(b) ? (a):(b))

int compare(const void *a, const void *b){
  int *ia = (int*) a;
  int *ib = (int*) b;
  if(ia[0]<ib[0]){
    return -1;
  }
  if(ia[0]>ib[0]){
    return 1;
  }
  return 0;
}

void serialCollatz(int N, int *counts){

  //#pragma omp parallel for
  for(int n=1;n<=N;++n){

    long long int a = n;

    int count = 0;

    while(a!=1 && a>0){

      if(a%2==0){
	a = a/2;
      }else{
	a = 3*a+1;
      }

      ++count;
    }

    counts[n-1] = (a>0) ? count:-count;
  }
}

__global__
void cudaCollatz(int N, int *counts){

  int n = threadIdx.x + blockDim.x*blockIdx.x;
  
  long long int a = n + 1;
  
  int count = 0;
  
  while(a!=1 && a>0){
    
    if(a%2==0){
      a = a/2;
    }else{
      a = 3*a+1;
    }
    
    ++count;
  }
  
  if(n<N){
    counts[n] = (a>0) ? count:-count;
  }

}


int main(int argc, char **argv){

  int N = atoi(argv[1]);

  int *h_countsCPU = (int*) calloc(N, sizeof(int));
  int *h_countsGPU = (int*) calloc(N, sizeof(int));

  int *c_counts;

  cudaMalloc(&c_counts, N*sizeof(int));


  clock_t tic, toc;

  
  tic = clock();
#if 0
  serialCollatz(N, h_countsCPU);
#endif
  toc = clock();

  float serialElapsed = (toc-tic)/(double)CLOCKS_PER_SEC;
  printf("Serial elapsed time = %g\n", serialElapsed);
  
  int B = 64;
  int G = (N+B-1)/B;

  cudaEvent_t start, end;

  cudaEventCreate(&start);
  cudaEventCreate(&end);

  cudaEventRecord(start);
  cudaCollatz <<< G, B >>> (N, c_counts);
  cudaEventRecord(end);

  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, start, end);
  elapsed /= 1000.;

  printf("CUDA   elapsed time = %g\n", elapsed);
  printf("SPEED UP = %g\n", serialElapsed/elapsed);

  cudaMemcpy(h_countsGPU, c_counts, N*sizeof(int), cudaMemcpyDeviceToHost);

  qsort(h_countsGPU, N, sizeof(int), compare);
  
  double meanCount = 0, maxCount = 0, minCount = 1e9;
  
  for(int n=0;n<N;++n){
    minCount = min(minCount, h_countsGPU[n]);
    maxCount = max(maxCount, h_countsGPU[n]);
    meanCount += h_countsGPU[n];
  }
  meanCount /= (double)N;
  printf("minCount = %f, maxCount = %f, meanCount = %f, medianCount = %f\n",
	 minCount, maxCount, meanCount, (double)h_countsGPU[N/2]);
  
    
  
#if 0
  for(int n=0;n<N;++n){
    if(h_countsGPU[n]!=h_countsCPU[n]){
      printf("uh oh - different answer %d to %d \n",
	     h_countsGPU[n], h_countsCPU[n]);
    }
  }
#endif
}
