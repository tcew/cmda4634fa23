/*

To compile:

On RTX 4090: nvcc --keep  -arch=sm_89 --use_fast_math -O3  -o cudaMandelbrot cudaMandelbrot.cu png_util.cpp -I. -lm -lpng  

On pascal:   nvcc --keep  -arch=sm_61 --use_fast_math -O3  -o cudaMandelbrot cudaMandelbrot.cu png_util.cpp -I. -lm -lpng  

To run:
./cudaMandelbrot

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cuda.h>

extern "C"
{
#include "png_util.h"
}

__global__ void mandelbrotKernelV0(const int Nx, 
				   const int Ny, 
				   const float minx,
				   const float miny,
				   const float hx, 
				   const float hy,
				   float * __restrict__ count){

  int n = blockIdx.x*blockDim.x + threadIdx.x;
  int m = blockIdx.y*blockDim.y + threadIdx.y;
  
  if(m<Ny){
    if(n<Nx){
      float cx = minx + n*hx;
      float cy = miny + m*hy;

      float zx = 0;
      float zy = 0;
      
#define p_Nt 200

      int t, cnt=0;

      for(t=0;t<p_Nt;++t){
	
	// z = z^2 + c
	//   = (zx + i*zy)*(zx + i*zy) + (cx + i*cy)
	//   = zx^2 - zy^2 + 2*i*zy*zx + cx + i*cy
	float zxTmp = zx*zx - zy*zy + cx;
	zy = 2.f*zy*zx + cy;
	zx = zxTmp;

	cnt += (zx*zx+zy*zy<4.f);
      }

      count[n + m*Nx] = cnt;
    }
  }
}


__global__ void mandelbrotKernelV1(const int Nx, 
				   const int Ny, 
				   const float minx,
				   const float miny,
				   const float hx, 
				   const float hy,
				   float * __restrict__ count){

  int n = blockIdx.x*blockDim.x + threadIdx.x;
  int m = blockIdx.y*blockDim.y + threadIdx.y;
  
  if(m<Ny){
    if(n<Nx){
      float cx = minx + n*hx;
      float cy = miny + m*hy;

      const float a = 2.f*cx;
      const float b = cy;
      
      float x = 0;
      float y = 0;

#define p_Nt 200
      int cnt=0;

#pragma unroll 100
      for(int t=0;t<p_Nt;++t){

	const float x1 = x-2.f*y; // FMA
	const float x2 = 0.5f*x+y; // FMA

	y = x*y + b;   // FMA
	x = x1*x2 + a; // FMA

	cnt += (0.25f*x*x+y*y<4.f);
      }
      
      count[n + m*Nx] = cnt;
    }
  }
}



__global__ void mandelbrotKernelV2(const int Nx, 
				   const int Ny, 
				   const float minx,
				   const float miny,
				   const float hx, 
				   const float hy,
				   float * __restrict__ count){

  int n = blockIdx.x*blockDim.x + threadIdx.x;
  int m = blockIdx.y*blockDim.y + threadIdx.y;
  
  if(m<Ny){
    if(n<Nx){
      const float cx = minx + n*hx;
      const float cy = miny + m*hy;

      const float a = 2.f*cx;
      const float b = cy;
      
      float x = 0;
      float y = 0;

#define p_Nt 200
      int t=0;

#pragma unroll 200
      for(t=0;t<p_Nt;++t){

	const float x1 = x + (-2.f)*y; // FMA
	const float x2 = 0.5f*x+y; // FMA

	y =   x*y + b;   // FMA
	x = x1*x2 + a; // FMA

	const float test = x*x+4.f*(y*y-4.f);
	if(test>0)
	  break;
      }
      
      count[n + m*Nx] = t+1;
    }
  }
}


__global__ void mandelbrotKernelV3(const int Nx, 
				   const int Ny, 
				   const float minx,
				   const float miny,
				   const float hx, 
				   const float hy,
				   float * __restrict__ count){

  int n = blockIdx.x*blockDim.x + threadIdx.x;
  int m = blockIdx.y*blockDim.y + threadIdx.y;
  
  if(m<Ny){
    if(n<Nx){
      float modcx = (minx + n*hx) - 4.f; // TW: MOD
      float modcy = 0.5f*(miny + m*hy); // TW: MOD

      float zx = 0;
      float zy = 0;
      
#define p_Nt 200
      
      unsigned int t;

      float zy2 = zy*zy - 4.f;

#pragma unroll 50
      for(t=0;t<p_Nt;++t){

	zy = 2.f*(zy*zx + modcy); // FMUL and FMA 

	zx = (zx*zx + modcx) - zy2; // FMA and FSUB

	zy2 = zy*zy - 4.f; // FMA
	
	const float test  = zx*zx+zy2; // FMA

	if(test>0)
	  break;
      }
      
      count[n + m*Nx] = t;
    }
  }
}


__global__ void mandelbrotKernelV4(const int Nx, 
				   const int Ny, 
				   const float minx,
				   const float miny,
				   const float hx, 
				   const float hy,
				   float * __restrict__ count){

  int n = blockIdx.x*blockDim.x + threadIdx.x;
  int m = blockIdx.y*blockDim.y + threadIdx.y;
  
  if(m<Ny){
    if(n<Nx){
      float modcx = (minx + n*hx) - 4.f; // TW: MOD
      float modcy = 0.5f*(miny + m*hy); // TW: MOD

      float zx = 0;
      float zy = 0;
      
#define p_Nt 200
      
      int t;

      float zy2 = __fmaf_rn(zy,zy, -4.f);

      
#pragma unroll 50
      for(t=0;t<p_Nt;++t){

	zy = 2.f*__fmaf_rn(zy,zx,modcy); // FMUL and FMA 

	zx = __fmaf_rn(zx,zx,modcx) - zy2; // FMA and FSUB

	zy2 = __fmaf_rn(zy,zy,-4.f); // FMA
	
	const float test  = __fmaf_rn(zx,zx,zy2); // FMA

	if(test>0)
	  break;
      }
      
      count[n + m*Nx] = t;
    }
  }
}





int main(int argc, char **argv){

  const int Nx = 8192;
  const int Ny = 8192;

  /* box containing sample points */
  float centx = -.759856, centy= .125547;
  float diam  = 0.151579;
  float minx = centx-0.5*diam;
  float remax = centx+0.5*diam;
  float miny = centy-0.5*diam;
  float immax = centy+0.5*diam;

  float dx = (remax-minx)/(Nx-1.f);
  float dy = (immax-miny)/(Ny-1.f);

  float *h_count = (float*) calloc(Nx*Ny, sizeof(float));
  float *c_count;
  cudaMalloc(&c_count, Nx*Ny*sizeof(float));
  
  // call mandelbrot from here
  dim3 T(8,16); // TW -  small blocks (reduce divergence), 2 warps
  dim3 G( (Nx+T.x-1)/T.x, (Ny+T.y-1)/T.y);

  int Ntests = 100;
  
  for(int test=0;test<Ntests;++test){
    mandelbrotKernelV0 <<< G, T >>> (Nx, Ny, minx, miny, dx, dy, c_count);
    mandelbrotKernelV1 <<< G, T >>> (Nx, Ny, minx, miny, dx, dy, c_count);
    mandelbrotKernelV2 <<< G, T >>> (Nx, Ny, minx, miny, dx, dy, c_count);
    mandelbrotKernelV3 <<< G, T >>> (Nx, Ny, minx, miny, dx, dy, c_count);
    mandelbrotKernelV4 <<< G, T >>> (Nx, Ny, minx, miny, dx, dy, c_count);
  }
  
  cudaDeviceSynchronize();
  cudaMemcpy(h_count, c_count, Nx*Ny*sizeof(int), cudaMemcpyDeviceToHost);
  
  FILE *png = fopen("mandelbrot.png", "w");
  write_hot_png(png, Nx, Ny, h_count, 0, 80);
  fclose(png);

}
