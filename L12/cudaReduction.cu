
#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>

// v0: horrible serial code
__global__ void reductionKernelV00(int N, float *x, float *sumx){

  // sum up N values from x
  float sum = 0;
  for(int n=0;n<N;++n){
    sum += x[n];
  }

  sumx[0] = sum;

}

// v1: half a vector  (run this log2(N) times)
__global__ void reductionKernelV01(int N, float *x, float *sumx){

  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N && n+N/2){
    sumx[n] = x[n] + x[n+N/2];
  }
  
}

#define p_T 1024

// v2: partial reduction by one thread using parallel loaded data
__global__ void reductionKernelV02(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t==0){
    float sum = 0;
    for(int m=0;m<p_T;++m){
      sum += s_x[m];
    }
    sumx[blockIdx.x] = sum;
  }
  
}


// v2: partial reduction by one thread using parallel loaded data
__global__ void reductionKernelV02(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t==0){
    float sum = 0;
    for(int m=0;m<p_T;++m){
      sum += s_x[m];
    }
    sumx[blockIdx.x] = sum;
  }
  
}

// v3: partial reduction by one thread using parallel loaded data
__global__ void reductionKernelV03(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t<512) s_x[t] += s_x[t+512];

  __syncthreads();

  if(t<256) s_x[t] += s_x[t+256];
  
  __syncthreads();

  if(t<128) s_x[t] += s_x[t+128];

  __syncthreads();

  if(t<64) s_x[t] += s_x[t+64];

  __syncthreads();

  if(t<32) s_x[t] += s_x[t+32];

  __syncthreads();

  if(t<16) s_x[t] += s_x[t+16];

  __syncthreads();
  
  if(t<8) s_x[t] += s_x[t+8];

  __syncthreads();

  if(t<4) s_x[t] += s_x[t+4];

  __syncthreads();

  if(t<2) s_x[t] += s_x[t+2];

  __syncthreads();

  if(t<1){
    s_x[t] += s_x[t+1];
    sumx[blockIdx.x] = s_x[0];
  }
}


// v4: throw away some synchronizations
__global__ void reductionKernelV04(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  volatile __shared__ float s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t<512) s_x[t] += s_x[t+512];

  __syncthreads();

  if(t<256) s_x[t] += s_x[t+256];
  
  __syncthreads();

  if(t<128) s_x[t] += s_x[t+128];

  __syncthreads();

  if(t<64) s_x[t] += s_x[t+64];

  __syncthreads();

  if(t<32) s_x[t] += s_x[t+32];

  if(t<16) s_x[t] += s_x[t+16];
  
  if(t<8) s_x[t] += s_x[t+8];

  if(t<4) s_x[t] += s_x[t+4];

  if(t<2) s_x[t] += s_x[t+2];

  if(t<1){
    s_x[t] += s_x[t+1];
    sumx[blockIdx.x] = s_x[0];
  }
}



// v5: only use warp sync and one thread-block sync
__global__ void reductionKernelV05(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  volatile __shared__ float s_x[32][32];
  volatile __shared__ float s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = threadIdx.x + 32*w + blockIdx.x*1024;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  if(t<16) s_x[w][t] += s_x[w][t+16];
  if(t< 8) s_x[w][t] += s_x[w][t+8];
  if(t< 4) s_x[w][t] += s_x[w][t+4];
  if(t< 2) s_x[w][t] += s_x[w][t+2];
  if(t< 1) s_x[w][t] += s_x[w][t+1];

  if(t==0){
    s_sumx[t] = s_x[w][0];
  }
  
  __syncthreads();

  if(w==0){
    if(t<16) s_sumx[t] += s_sumx[t+16];
    if(t<8)  s_sumx[t] += s_sumx[t+8];
    if(t<4)  s_sumx[t] += s_sumx[t+4];
    if(t<2)  s_sumx[t] += s_sumx[t+2];
    if(t<1)  s_sumx[t] += s_sumx[t+1];

    if(t==0){
      sumx[blockIdx.x] = s_sumx[0];
    }
  }
}


// v6: use warpsync
__global__ void reductionKernelV06(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[32][32];
  __shared__ float s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = threadIdx.x + 32*w + blockIdx.x*1024;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  if(t<16) s_x[w][t] += s_x[w][t+16];
  __syncwarp();
  if(t< 8) s_x[w][t] += s_x[w][t+8];
  __syncwarp();
  if(t< 4) s_x[w][t] += s_x[w][t+4];
  __syncwarp();
  if(t< 2) s_x[w][t] += s_x[w][t+2];
  __syncwarp();
  if(t< 1) s_x[w][t] += s_x[w][t+1];
  __syncwarp();

  if(t==0){
    s_sumx[t] = s_x[w][0];
  }
  
  __syncthreads();

  if(w==0){
    if(t<16) s_sumx[t] += s_sumx[t+16];
    __syncwarp();
    if(t<8)  s_sumx[t] += s_sumx[t+8];
    __syncwarp();
    if(t<4)  s_sumx[t] += s_sumx[t+4];
    __syncwarp();    
    if(t<2)  s_sumx[t] += s_sumx[t+2];
    __syncwarp();
    if(t<1)  s_sumx[t] += s_sumx[t+1];

    if(t==0){
      sumx[blockIdx.x] = s_sumx[0];
    }
  }
}


// v7: pipeline data loads at start
__global__ void reductionKernelV07(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[32][32];
  __shared__ float s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = threadIdx.x + 32*w + blockIdx.x*1024;

  if(n<N){
    //    s_x[t] = x[n];
    int m = n;
    float tmp = 0;
    while(m<N){
      tmp += x[m];
      m += threadDim.x*threadDim.y*blockIdx.x;
    }
    s_x[t] = tmp;
  }

  // barrier all threads in this thread-block
  if(t<16) s_x[w][t] += s_x[w][t+16];
  __syncwarp();
  if(t< 8) s_x[w][t] += s_x[w][t+8];
  __syncwarp();
  if(t< 4) s_x[w][t] += s_x[w][t+4];
  __syncwarp();
  if(t< 2) s_x[w][t] += s_x[w][t+2];
  __syncwarp();
  if(t< 1) s_x[w][t] += s_x[w][t+1];
  __syncwarp();

  if(t==0){
    s_sumx[t] = s_x[w][0];
  }
  
  __syncthreads();

  if(w==0){
    if(t<16) s_sumx[t] += s_sumx[t+16];
    __syncwarp();
    if(t<8)  s_sumx[t] += s_sumx[t+8];
    __syncwarp();
    if(t<4)  s_sumx[t] += s_sumx[t+4];
    __syncwarp();    
    if(t<2)  s_sumx[t] += s_sumx[t+2];
    __syncwarp();
    if(t<1)  s_sumx[t] += s_sumx[t+1];

    if(t==0){
      sumx[blockIdx.x] = s_sumx[0];
    }
  }
}

// atomic
__global__ void reductionKernelV07(int N, float *x, float *sumx){

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = threadIdx.x + 32*w + blockIdx.x*1024;

  if(n<N){
    //    s_x[t] = x[n];
    int m = n;
    float tmp = 0;
    while(m<N){
      tmp += x[m];
      m += threadDim.x*threadDim.y*blockIdx.x;
    }
    atomicAdd(sumx, tmp);
  }
}
  




















