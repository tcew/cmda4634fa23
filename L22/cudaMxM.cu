#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cublas_v2.h>

#define dfloat  float
#define dfloat2 float2
#define dfloat4 float4

// MULTIPLY C = A*B
// C is MxN
// A is M*K
// B is K*N

// https://siboehm.com/articles/22/CUDA-MMM
// https://tinyurl.com/4j585f8c

__global__ void mxmKernelV01(int M, int K, int N, dfloat *A, dfloat *B, dfloat *C){
  
  int r = threadIdx.y + blockIdx.y*blockDim.y;
  int c = threadIdx.x + blockIdx.x*blockDim.x;
  
  dfloat res = 0;
  if(r<M && c<N){
    for(int n=0;n<K;++n){
      res += A[r*K + n]*B[n*N + c];
    }
    
    C[r*N + c] = res;
  }
}

#define BLK 32
// preload blocks of A and B to shared
__global__ void __launch_bounds__(BLK*BLK) mxmKernelV02(const int M, const int K, const int N,
							const dfloat * __restrict__ A,
							const dfloat * __restrict B, dfloat * __restrict__ C){

  int tn = threadIdx.x;
  int tm = threadIdx.y;

  int bn = blockIdx.x;
  int bm = blockIdx.y;
  
  int Am = tm + bm*blockDim.y;
  int Bn = tn + bn*blockDim.x;
  
  dfloat res = 0;
  
  __shared__ dfloat s_A[BLK][BLK];
  __shared__ dfloat s_B[BLK][BLK];
  
  for(int bk=0;bk<K;bk+=BLK){
    
    __syncthreads();
    
    int An = bk+tn;
    int Bm = bk+tm;
    
    s_A[tm][tn] =  A[Am*K + An];
    s_B[tm][tn] =  B[Bm*N + Bn];
    
    __syncthreads();
    
#pragma unroll
    for(int k=0;k<BLK;++k){
      res += s_A[tm][k]*s_B[k][tn];
    }
  }
  
  if(Am<M && Bn<N)
    C[Am*N + Bn] = res;
  
}


#if 1

/* size of thread-block */
#define p_TM 4
#define p_TN 16

/* block A rows */
#define p_BM 64
/* block A cols */
#define p_BK 16
/* block B cols */
#define p_BN 64
#endif

#if 0
// GOOD 

/* size of thread-block */
#define p_TM 8
#define p_TN 8

/* block A rows */
#define p_BM 64
/* block A cols */
#define p_BK 16
/* block B cols */
#define p_BN 64

#endif

/*
  MUST SATISFY:
  1.  p_BM*p_BK is a multiple of p_TM*p_TN 
  2.  p_BN*p_BK is a multiple of p_TM*p_TN 
*/

/* total number of threads per thread-block */
#define p_NUMTHREADS (p_TM*p_TN)

/* number of thread-local registers in each direction */
#define p_RM (p_BM/p_TM)
#define p_RN (p_BN/p_TN)


__global__ void __launch_bounds__(p_RM*p_RN) mxmKernelV03(int M, int K, int N, dfloat *A, dfloat *B, dfloat *C){

  int bn = blockIdx.x;
  int bm = blockIdx.y;
  int t = threadIdx.x; 

  A += bm*p_BM*K; // shift to start of A row block
  B += bn*p_BN;   // shift to start of B column block
  C += bm*p_BM*N + bn*p_BN; // shift to output block of C
  
  int tblkn = t%p_TN;
  int tblkm = t/p_TN;
  
  __shared__ dfloat s_A[p_BM][p_BK];
  __shared__ dfloat s_B[p_BK][p_BN];

  dfloat r_C[p_RM][p_RN] = {0.f};
 
  for(int blk=0;blk<K;blk+=p_BK){
    
    __syncthreads();

    int load;
#if (p_BM*p_BK)>=p_NUMTHREADS
    load = t;
    for(int l=0;l<(p_BM*p_BK)/p_NUMTHREADS;++l){
      int cA = load%p_BK;
      int rA = load/p_BK;

      if(rA<p_BM && cA<p_BK)
	s_A[rA][cA] = A[rA*K+cA];
      
      load += p_NUMTHREADS;
    }
#else
    if(t<p_BM*p_BK){
      int cA = t%p_BK;
      int rA = t/p_BK;

      if(rA<p_BM && cA<p_BK)
	s_A[rA][cA] = A[rA*K+cA];
    }
#endif

#if (p_BN*p_BK)>=p_NUMTHREADS
    load = t;
    for(int l=0;l<(p_BM*p_BK)/p_NUMTHREADS;++l){
      int cB = load%p_BN;
      int rB = load/p_BN;
      load += p_NUMTHREADS;
      if(rB<p_BK && cB<p_BN)
	s_B[rB][cB] = B[rB*N+cB];
    }
#else
    if(t<p_BN*p_BK){
      int cB = t%p_BN;
      int rB = t/p_BN;

      if(rB<p_BK && cB<p_BN)
	s_B[rB][cB] = B[rB*N+cB];
    }
#endif
    
    __syncthreads();

    
    // now do multiply 

    for(int rm=0;rm<p_RM;++rm){
      for(int rn=0;rn<p_RN;++rn){

	int sm = tblkm*p_RM + rm;
	int sn = tblkn*p_RN + rn;
	
	for(int sk=0;sk<p_BK;++sk){
	  r_C[rm][rn] += s_A[sm][sk]*s_B[sk][sn];
	}
      }
    }
    
    A += p_BK; // shift A p_BK columns right
    B += p_BK*N; // shift B p_BK rows down
    
  }

  for(int rm=0;rm<p_RM;++rm){
    for(int rn=0;rn<p_RN;++rn){
      
      int sm = tblkm*p_RM + rm;
      int sn = tblkn*p_RN + rn;
      
      C[sm*N+sn] = r_C[rm][rn];
    }
  }
}

// swap order of loops

__global__ void __launch_bounds__(p_RM*p_RN) mxmKernelV04(int M, int K, int N, dfloat *A, dfloat *B, dfloat *C){

  int bn = blockIdx.x;
  int bm = blockIdx.y;
  int t = threadIdx.x; 

  A += bm*p_BM*K; // shift to start of row block
  B += bn*p_BN;   // shift to start of column block
  C += bm*p_BM*N + bn*p_BN;
  
  int tblkn = t%p_TN;
  int tblkm = t/p_TN;
  
  __shared__ dfloat s_A[p_BM][p_BK];
  __shared__ dfloat s_B[p_BK][p_BN];

  dfloat r_C[p_RM][p_RN] = {0.f};
 
  for(int blk=0;blk<K;blk+=p_BK){
    
    __syncthreads();

    // GOOD 
    int load = t;
    for(int l=0;l<(p_BM*p_BK)/p_NUMTHREADS;++l){
      int cA = load%p_BK;
      int rA = load/p_BK;
      s_A[rA][cA] = A[rA*K+cA];
      load += p_NUMTHREADS;
    }

    load = t;
    for(int l=0;l<(p_BK*p_BN)/p_NUMTHREADS;++l){
      int cB = load%p_BN;
      int rB = load/p_BN;
      load += p_NUMTHREADS;
      s_B[rB][cB] = B[rB*N+cB];
    }
    
    __syncthreads();
    
    // now do multiply 

    for(int sk=0;sk<p_BK;++sk){
      for(int rm=0;rm<p_RM;++rm){
	for(int rn=0;rn<p_RN;++rn){
	  
	  int sm = tblkm*p_RM + rm;
	  int sn = tblkn*p_RN + rn;
	  r_C[rm][rn] += s_A[sm][sk]*s_B[sk][sn];
	}
      }
    }
    
    A += p_BK; // shift A p_BK columns right
    B += p_BK*N; // shift B p_BK rows down
    
  }

  for(int rm=0;rm<p_RM;++rm){
    for(int rn=0;rn<p_RN;++rn){
      
      int sm = tblkm*p_RM + rm;
      int sn = tblkn*p_RN + rn;
      
      C[sm*N+sn] = r_C[rm][rn];
    }
  }
}

// prefetch to registers
__global__ void __launch_bounds__(p_RM*p_RN)  mxmKernelV05(int M, int K, int N, dfloat *A, dfloat *B, dfloat *C){

  int bn = blockIdx.x;
  int bm = blockIdx.y;
  int t = threadIdx.x; 

  A += bm*p_BM*K; // shift to start of row block
  B += bn*p_BN;   // shift to start of column block
  C += bm*p_BM*N + bn*p_BN;
  
  int tblkn = t%p_TN;
  int tblkm = t/p_TN;
  
  __shared__ dfloat s_A[p_BM][p_BK];
  __shared__ dfloat s_B[p_BK][p_BN];

  dfloat r_C[p_RM][p_RN] = {0.f};
 
  for(int blk=0;blk<K;blk+=p_BK){
    
    __syncthreads();

#if 1
    int loadA = t;
#pragma unroll
    for(int l=0;l<(p_BM*p_BK)/p_NUMTHREADS;++l){
      int cA = loadA%p_BK;
      int rA = loadA/p_BK;
      loadA += p_NUMTHREADS;
      s_A[rA][cA] = A[rA*K+cA];
    }

    int loadB = t;
#pragma unroll
    for(int l=0;l<(p_BK*p_BN)/p_NUMTHREADS;++l){
      int cB = loadB%p_BN;
      int rB = loadB/p_BN;
      loadB += p_NUMTHREADS;
      s_B[rB][cB] = B[rB*N+cB];
    }
#endif

#if 0
    // BAD
    int load = t;
    
    while(load<p_BM*p_BK){
      int cA = load%p_BK;
      int rA = load/p_BK;
      load += p_NUMTHREADS;
      
      s_A[rA][cA] = A[rA*K+cA];
    }

    load = t;

    while(load<p_BN*p_BK){
      int cB = load%p_BN;
      int rB = load/p_BN;
      load += p_NUMTHREADS;
      s_B[rB][cB] = B[rB*N+cB];
    }
#endif

    __syncthreads();
    
    // now do multiply 

    dfloat tmpA[p_RM], tmpB[p_RN];
    
    for(int sk=0;sk<p_BK;++sk){
      for(int rm=0;rm<p_RM;++rm){
	int sm = tblkm*p_RM + rm;
	tmpA[rm] = s_A[sm][sk];
      }
      for(int rn=0;rn<p_RN;++rn){
	int sn = tblkn*p_RN + rn;
	tmpB[rn] = s_B[sk][sn];
      }
      
      for(int rm=0;rm<p_RM;++rm){
	for(int rn=0;rn<p_RN;++rn){
	  r_C[rm][rn] += tmpA[rm]*tmpB[rn];
	}
      }
    }
    
    A += p_BK; // shift A p_BK columns right
    B += p_BK*N; // shift B p_BK rows down
    
  }

  for(int rm=0;rm<p_RM;++rm){
    for(int rn=0;rn<p_RN;++rn){
      
      int sm = tblkm*p_RM + rm;
      int sn = tblkn*p_RN + rn;
      
      C[sm*N+sn] = r_C[rm][rn];
    }
  }
}

// unroll 
__global__ void __launch_bounds__(p_RM*p_RN) mxmKernelV06(int M, int K, int N, dfloat *A, dfloat *B, dfloat *C){

  int bn = blockIdx.x;
  int bm = blockIdx.y;
  int t = threadIdx.x; 

  A += bm*p_BM*K; // shift to start of row block
  B += bn*p_BN;   // shift to start of column block
  C += bm*p_BM*N + bn*p_BN;
  
  int tblkn = t%p_TN;
  int tblkm = t/p_TN;
  
  __shared__ dfloat s_A[p_BM][p_BK+1];
  __shared__ dfloat s_B[p_BK][p_BN];

  dfloat r_C[p_RM][p_RN] = {0.f};

#pragma unroll 1
  for(int blk=0;blk<K;blk+=p_BK){
    
    __syncthreads();

#if 0

    int load = t;

#pragma unroll
    for(int l=0;l<(p_BM*p_BK)/p_NUMTHREADS;++l){
      int cA = load%p_BK;
      int rA = load/p_BK;
      load += p_NUMTHREADS;
      
      s_A[rA][cA] = A[rA*K+cA]; // stride is K per row in A
    }

    load = t;

#pragma unroll
    for(int l=0;l<(p_BK*p_BN)/p_NUMTHREADS;++l){
      int cB = load%p_BN;
      int rB = load/p_BN;
      load += p_NUMTHREADS;
      s_B[rB][cB] = B[rB*N+cB]; // stride is N per row in B
    }

#else

    
    int load = t;

#pragma unroll
    for(int l=0;l<(p_BM*p_BK)/p_NUMTHREADS;++l){
      int cA = load%p_BK;
      int rA = load/p_BK;
      load += p_NUMTHREADS;
      
      s_A[rA][cA] = A[rA*K+cA]; // stride is K per row in A
    }

    load = t;

#pragma unroll
    for(int l=0;l<(p_BK*p_BN)/p_NUMTHREADS;++l){
      int cB = load%p_BN;
      int rB = load/p_BN;
      load += p_NUMTHREADS;
      s_B[rB][cB] = B[rB*N+cB]; // stride is N per row in B
    }

#endif
    
    __syncthreads();
    
    // now do multiply 

#pragma unroll
    for(int sk=0;sk<p_BK;++sk){

      dfloat tmpB[p_RN];

      int sn = tblkn*p_RN;
      for(int rn=0;rn<p_RN;++rn){
	tmpB[rn] = s_B[sk][sn++]; // sn is the same for all threads in the same TN group
      }

      int sm = tblkm*p_RM ;
#pragma unroll
      for(int rm=0;rm<p_RM;++rm){
	dfloat Atmp = s_A[sm++][sk]; // broadcast to all threads in warp
	for(int rn=0;rn<p_RN;++rn){
	  r_C[rm][rn] += Atmp*tmpB[rn];
	}
      }
    }
    
    A += p_BK; // shift A p_BK columns right
    B += p_BK*N; // shift B p_BK rows down
    
  }

  for(int rm=0;rm<p_RM;++rm){
    for(int rn=0;rn<p_RN;++rn){
      
      int sm = tblkm*p_RM + rm;
      int sn = tblkn*p_RN + rn;
      
      C[sm*N+sn] = r_C[rm][rn];
    }
  }
}


// float4 loader ?
__global__ void __launch_bounds__(p_RM*p_RN) mxmKernelV07(int M, int K, int N, dfloat *A, dfloat *B, dfloat *C){

  int bn = blockIdx.x;
  int bm = blockIdx.y;
  int t = threadIdx.x; 

  A += bm*p_BM*K; // shift to start of row block
  B += bn*p_BN;   // shift to start of column block
  C += bm*p_BM*N + bn*p_BN;
  
  int tblkn = t%p_TN;
  int tblkm = t/p_TN;
  
  __shared__ dfloat s_A[p_BM][p_BK+1];
  __shared__ dfloat s_B[p_BK][p_BN];

  dfloat r_C[p_RM][p_RN] = {0.f};

#pragma unroll 1
  for(int blk=0;blk<K;blk+=p_BK){
    
    __syncthreads();

    
    int load = t;

#pragma unroll
    for(int l=0;l<(p_BM*p_BK)/(2*p_NUMTHREADS);++l){
      int cA = load%(p_BK/2);
      int rA = load/(p_BK/2);
      load += p_NUMTHREADS;
      dfloat2 tmpA = ((dfloat2*)A)[rA*(K/2)+cA];
      s_A[rA][2*cA+0] = tmpA.x;
      s_A[rA][2*cA+1] = tmpA.y;
    }

    load = t;

#pragma unroll 
    for(int l=0;l<(p_BK*p_BN)/(p_NUMTHREADS);++l){
      int cB = load%(p_BN);
      int rB = load/(p_BN);
      load += p_NUMTHREADS;
      const dfloat tmpB = ((dfloat*)B)[rB*(N)+cB];

      s_B[rB][cB] = tmpB;
    }


    
    __syncthreads();
    
    // now do multiply 

#pragma unroll
    for(int sk=0;sk<p_BK;++sk){

      dfloat tmpB[p_RN];

      int sn = tblkn*p_RN;
#pragma unroll
      for(int rn=0;rn<p_RN;++rn){
	tmpB[rn] = s_B[sk][sn++]; // sn is the same for all threads in the same TN group
      }

      int sm = tblkm*p_RM ;
#pragma unroll
      for(int rm=0;rm<p_RM;++rm){
	const dfloat Atmp = s_A[sm++][sk]; // broadcast to all threads in warp
#pragma unroll
	for(int rn=0;rn<p_RN;++rn){
	  r_C[rm][rn] += Atmp*tmpB[rn];
	}
      }
    }
    
    A += p_BK; // shift A p_BK columns right
    B += p_BK*N; // shift B p_BK rows down
    
  }

#pragma unroll
  for(int rm=0;rm<p_RM;++rm){
  	  #pragma unroll
    for(int rn=0;rn<p_RN;++rn){
      
      int sm = tblkm*p_RM + rm;
      int sn = tblkn*p_RN + rn;
      
      C[sm*N+sn] = r_C[rm][rn];
    }
  }
}

// specialized
__global__ void __launch_bounds__(p_RM*p_RN) mxmKernelV08(int M, int K, int N, dfloat *A, dfloat *B, dfloat *C){

  int bn = blockIdx.x;
  int bm = blockIdx.y;
  int t = threadIdx.x; 

  A += bm*p_BM*K; // shift to start of row block
  B += bn*p_BN;   // shift to start of column block
  C += bm*p_BM*N + bn*p_BN;
  
  int tblkn = t%p_TN;
  int tblkm = t/p_TN;
  
  __shared__ dfloat s_A[p_BM][p_BK+1];
  __shared__ dfloat4 s_B4[p_BK][p_BN];

  dfloat4 r_C4[p_RM] = {0.f};

#pragma unroll 2
  for(int blk=0;blk<K;blk+=p_BK){
    
    __syncthreads();
    
    int load = t;

    int cA = load & 3;

#pragma unroll 4
    for(int l=0;l<4;++l){
      int rA = load >> 2;
      load += p_NUMTHREADS;
      dfloat4 tmpA = ((dfloat4*)A)[rA*(K/4)+cA];
      s_A[rA][4*cA+0] = tmpA.x;
      s_A[rA][4*cA+1] = tmpA.y;
      s_A[rA][4*cA+2] = tmpA.z;
      s_A[rA][4*cA+3] = tmpA.w;
    }

    load = t;

    int cB = load&15;
#pragma unroll 4
    for(int l=0;l<4;++l){
      int rB = load>>4;
      load += p_NUMTHREADS;
      const dfloat4 tmpB = ((dfloat4*)B)[rB*(N/4)+cB];

      s_B4[rB][cB] = tmpB;
    }
    
    __syncthreads();
    
    // now do multiply 

#pragma unroll 16
    for(int sk=0;sk<p_BK;++sk){

      dfloat4 tmpB= s_B4[sk][tblkn];

      int sm = tblkm*p_RM ;
#pragma unroll 16
      for(int rm=0;rm<p_RM;++rm){
	const dfloat Atmp = s_A[sm++][sk]; // broadcast to all threads in warp
	r_C4[rm].x += Atmp*tmpB.x;
	r_C4[rm].y += Atmp*tmpB.y;
	r_C4[rm].z += Atmp*tmpB.z;
	r_C4[rm].w += Atmp*tmpB.w;			
      }
    }
    
    A += p_BK; // shift A p_BK columns right
    B += p_BK*N; // shift B p_BK rows down
    
  }

#pragma unroll 16
  for(int rm=0;rm<p_RM;++rm){
    int sm = tblkm*p_RM + rm;
    int sn = tblkn*p_RN ;

    ((dfloat4*)C)[(sm*N+sn)>>2] = r_C4[rm];
  }
}


__global__ void __launch_bounds__(p_RM*p_RN) mxmKernelV09(int M, int K, int N, dfloat4 *A4, dfloat4 *B4, dfloat4 *C4){

  int bn = blockIdx.x;
  int bm = blockIdx.y;
  int t = threadIdx.x; 

  A4 += bm*(p_BM/4)*K; // shift to start of row block
  B4 += bn*(p_BN/4);   // shift to start of column block
  C4 += bm*(p_BM/4)*N + bn*(p_BN/4);
  
  int tblkn = t%p_TN;
  int tblkm = t/p_TN;
  
  __shared__ dfloat s_A[p_BM][p_BK+1];
  __shared__ dfloat4 s_B4[p_BK][p_BN];

  dfloat4 r_C4[p_RM] = {0.f};

#pragma unroll 2
  for(int blk=0;blk<K;blk+=p_BK){
    
    __syncthreads();
    
    int cA = t & 3;
    int cB = t & 15;

#pragma unroll 4
    for(int l=0;l<4;++l){
      int rA = (t+p_NUMTHREADS*l) >> 2;

      const dfloat4 tmpA = A4[rA*(K/4)+cA];

#if 1
      s_A[rA][4*cA+0] = tmpA.x;
      s_A[rA][4*cA+1] = tmpA.y;
      s_A[rA][4*cA+2] = tmpA.z;
      s_A[rA][4*cA+3] = tmpA.w;
#else
      // garbage with recast because of padding
      (reinterpret_cast<dfloat4*>(s_A[rA]))[cA] = tmpA;
#endif
    }

#pragma unroll 4
    for(int l=0;l<4;++l){
      int rB = (t+p_NUMTHREADS*l) >> 4;
      const dfloat4 tmpB = B4[rB*(N/4)+cB];
      
      s_B4[rB][cB] = tmpB;
    }
    
    __syncthreads();
    
    // now do multiply 

#pragma unroll 16
    for(int sk=0;sk<p_BK;++sk){
      
      const dfloat4 tmpB = s_B4[sk][tblkn];
      
      int sm = tblkm*p_RM ;
#pragma unroll 16
      for(int rm=0;rm<p_RM;++rm){
	const dfloat Atmp = s_A[sm++][sk]; // broadcast to all threads in warp

	r_C4[rm].x += Atmp*tmpB.x;
	r_C4[rm].y += Atmp*tmpB.y;
	r_C4[rm].z += Atmp*tmpB.z;
	r_C4[rm].w += Atmp*tmpB.w;			
      }
    }
    
    A4 += (p_BK/4); // shift A p_BK columns right
    B4 += (p_BK/4)*N; // shift B p_BK rows down
    
  }

#pragma unroll 16
  for(int rm=0;rm<p_RM;++rm){
    int sm = tblkm*p_RM + rm;
    int sn = tblkn*p_RN ;

    C4[(sm*N+sn)>>2] = r_C4[rm];
  }
}


// unroll 
__global__ void __launch_bounds__(p_RM*p_RN) mxmKernelV10(int M, int K, int N, dfloat *A, dfloat *B, dfloat *C){

  int bn = blockIdx.x;
  int bm = blockIdx.y;
  int t = threadIdx.x; 

  A += bm*p_BM*K; // shift to start of row block
  B += bn*p_BN;   // shift to start of column block
  C += bm*p_BM*N + bn*p_BN;
  
  int tblkn = t%p_TN;
  int tblkm = t/p_TN;

  __shared__ dfloat s_A[p_BM][p_BK+1];
  __shared__ dfloat s_B[p_BK][p_BN];

  dfloat r_C[p_RM][p_RN] = {0.f};

#pragma unroll 1
  for(int blk=0;blk<K;blk+=p_BK){
    
    __syncthreads();
    
    int load = t;

#pragma unroll
    for(int l=0;l<(p_BM*p_BK)/p_NUMTHREADS;++l){
      int cA = load%p_BK;
      int rA = load/p_BK;
      load += p_NUMTHREADS;
      
      s_A[rA][cA] = A[rA*K+cA]; // stride is K per row in A
    }

    load = t;

#pragma unroll
    for(int l=0;l<(p_BK*p_BN)/p_NUMTHREADS;++l){
      int cB = load%p_BN;
      int rB = load/p_BN;
      load += p_NUMTHREADS;
      s_B[rB][cB] = B[rB*N+cB]; // stride is N per row in B
    }
    
    __syncthreads();
    
    // now do multiply 

#pragma unroll 16
    for(int sk=0;sk<p_BK;++sk){

      dfloat tmpB[p_RN];

#pragma unroll 16
      for(int rn=0;rn<p_RN;++rn){
	tmpB[rn] = s_B[sk][16*rn+tblkn]; // sn is the same for all threads in the same TN group
      }
      
#pragma unroll 16
      for(int rm=0;rm<p_RM;++rm){
	dfloat Atmp = s_A[4*rm+tblkm][sk]; // broadcast to all threads in warp
#pragma unroll 4
	for(int rn=0;rn<p_RN;++rn){
	  r_C[rm][rn] += Atmp*tmpB[rn];
	}
      }
    }
    
    A += p_BK; // shift A p_BK columns right
    B += p_BK*N; // shift B p_BK rows down
    
  }

  __syncthreads();

  for(int rm=0;rm<p_RM;++rm){
    for(int rn=0;rn<p_RN;++rn){

      // terrible write pattern
      int sm =  4*rm+tblkm; // tblkm*p_RM + rm;
      int sn = 16*rn+tblkn;
      
      C[sm*N+sn] = r_C[rm][rn];
    }
  }
  
}



  
#define CEIL_DIV(M, N) (((M) + (N)-1) / (N))
const int WARPSIZE = 32; // warpSize is not constexpr

/*
 * @tparam BM The threadblock size for M dimension SMEM caching.
 * @tparam BN The threadblock size for N dimension SMEM caching.
 * @tparam BK The threadblock size for K dimension SMEM caching.
 * @tparam WM M dim of continuous tile computed by each warp
 * @tparam WN N dim of continuous tile computed by each warp
 * @tparam WMITER The number of subwarp tiling steps in M dimension.
 * @tparam WNITER The number of subwarp tiling steps in N dimension.
 * @tparam TM The per-thread tile size for M dimension.
 * @tparam TN The per-thread tile size for N dimension.
 */

// https://github.com/siboehm/SGEMM_CUDA/blob/master/src/runner.cu

const uint NUM_THREADS = 128;
const uint BN = 128;
const uint BM = 128;
const uint BK = 16;
const uint WN = 64;
const uint WM = 64;
const uint WNITER = 4;
const uint TN = 4;
const uint TM = 8;

__global__ void mxmKernelVINFTY(int M, int K, int N, dfloat *A, dfloat *B, dfloat *C) {

  const uint cRow = blockIdx.y;
  const uint cCol = blockIdx.x;

  // Placement of the warp in the threadblock tile
  const uint warpIdx = threadIdx.x / WARPSIZE; // the warp this thread is in
  const uint warpCol = warpIdx % (BN / WN);
  const uint warpRow = warpIdx / (BN / WN);

  // size of the warp subtile
  constexpr uint WMITER = (WM * WN) / (WARPSIZE * TM * TN * WNITER);
  constexpr uint WSUBM = WM / WMITER; // 64/2=32
  constexpr uint WSUBN = WN / WNITER; // 32/2=16

  // Placement of the thread in the warp subtile
  const uint threadIdxInWarp = threadIdx.x % WARPSIZE;         // [0, 31]
  const uint threadColInWarp = threadIdxInWarp % (WSUBN / TN); // i%(16/4)
  const uint threadRowInWarp = threadIdxInWarp / (WSUBN / TN); // i/4

  // allocate space for the current blocktile in SMEM
  __shared__ dfloat As[BM * BK];
  __shared__ dfloat Bs[BK * BN];

  // Move blocktile to beginning of A's row and B's column
  A += cRow * BM * K;
  B += cCol * BN;
  // Move C_ptr to warp's output tile
  C += (cRow * BM + warpRow * WM) * N + cCol * BN + warpCol * WN;

  // calculating the indices that this thread will load into SMEM
  // we'll load 128bit / 32bit = 4 elements per thread at each step
  const uint innerRowA = threadIdx.x / (BK / 4);
  const uint innerColA = threadIdx.x % (BK / 4);
  constexpr uint rowStrideA = (NUM_THREADS * 4) / BK;
  const uint innerRowB = threadIdx.x / (BN / 4);
  const uint innerColB = threadIdx.x % (BN / 4);
  constexpr uint rowStrideB = NUM_THREADS / (BN / 4);

  // allocate thread-local cache for results in registerfile
  dfloat threadResults[WMITER * TM * WNITER * TN] = {0.0};
  // we cache into registers on the warptile level
  dfloat regM[WMITER * TM] = {0.0};
  dfloat regN[WNITER * TN] = {0.0};

  // outer-most loop over block tiles
  for (uint bkIdx = 0; bkIdx < K; bkIdx += BK) {
    for (uint offset = 0; offset + rowStrideA <= BM; offset += rowStrideA) {
      dfloat4 tmp = reinterpret_cast<dfloat4 *>(
          &A[(innerRowA + offset) * K + innerColA * 4])[0];
      // transpose A while storing it
      As[(innerColA * 4 + 0) * BM + innerRowA + offset] = tmp.x;
      As[(innerColA * 4 + 1) * BM + innerRowA + offset] = tmp.y;
      As[(innerColA * 4 + 2) * BM + innerRowA + offset] = tmp.z;
      As[(innerColA * 4 + 3) * BM + innerRowA + offset] = tmp.w;
    }

    for (uint offset = 0; offset + rowStrideB <= BK; offset += rowStrideB) {
      reinterpret_cast<dfloat4 *>(
          &Bs[(innerRowB + offset) * BN + innerColB * 4])[0] =
          reinterpret_cast<dfloat4 *>(
              &B[(innerRowB + offset) * N + innerColB * 4])[0];
    }
    __syncthreads();

    for (uint dotIdx = 0; dotIdx < BK; ++dotIdx) {
      // populate registers for whole warptile
      for (uint wSubRowIdx = 0; wSubRowIdx < WMITER; ++wSubRowIdx) {
        for (uint i = 0; i < TM; ++i) {
          regM[wSubRowIdx * TM + i] =
              As[(dotIdx * BM) + warpRow * WM + wSubRowIdx * WSUBM +
                 threadRowInWarp * TM + i];
        }
      }
      for (uint wSubColIdx = 0; wSubColIdx < WNITER; ++wSubColIdx) {
        for (uint i = 0; i < TN; ++i) {
          regN[wSubColIdx * TN + i] =
              Bs[(dotIdx * BN) + warpCol * WN + wSubColIdx * WSUBN +
                 threadColInWarp * TN + i];
        }
      }

      // execute warptile matmul
      for (uint wSubRowIdx = 0; wSubRowIdx < WMITER; ++wSubRowIdx) {
        for (uint wSubColIdx = 0; wSubColIdx < WNITER; ++wSubColIdx) {
          // calculate per-thread results
          for (uint resIdxM = 0; resIdxM < TM; ++resIdxM) {
            for (uint resIdxN = 0; resIdxN < TN; ++resIdxN) {
              threadResults[(wSubRowIdx * TM + resIdxM) * (WNITER * TN) +
                            (wSubColIdx * TN) + resIdxN] +=
                  regM[wSubRowIdx * TM + resIdxM] *
                  regN[wSubColIdx * TN + resIdxN];
            }
          }
        }
      }
    }
    A += BK;     // move BK columns to right
    B += BK * N; // move BK rows down
    __syncthreads();
  }

  // write out the results
  for (uint wSubRowIdx = 0; wSubRowIdx < WMITER; ++wSubRowIdx) {
    for (uint wSubColIdx = 0; wSubColIdx < WNITER; ++wSubColIdx) {
      // move C pointer to current warp subtile
      dfloat *C_interim = C + (wSubRowIdx * WSUBM) * N + wSubColIdx * WSUBN;
      for (uint resIdxM = 0; resIdxM < TM; resIdxM += 1) {
        for (uint resIdxN = 0; resIdxN < TN; resIdxN += 4) {
          // load C vector into registers
          dfloat4 tmp = {0.}; 
          // perform GEMM update in reg
          const int i = (wSubRowIdx * TM + resIdxM) * (WNITER * TN) +
                        wSubColIdx * TN + resIdxN;
          tmp.x = threadResults[i + 0];
          tmp.y = threadResults[i + 1];
          tmp.z = threadResults[i + 2];
          tmp.w = threadResults[i + 3];
          // write back
          reinterpret_cast<dfloat4 *>(
              &C_interim[(threadRowInWarp * TM + resIdxM) * N +
                         threadColInWarp * TN + resIdxN])[0] = tmp;
        }
      }
    }
  }
}





void mxm( cublasHandle_t &handle, int version, int M, int K, int N, dfloat *A, dfloat *B, dfloat *C){

  switch(version){
  case 1:
    {
      int T = 32;
      dim3 G( (N + T-1)/T, (M + T-1)/T);
      dim3 TB(T,T);
      
      mxmKernelV01 <<< G, TB >>> (M, K, N, A, B, C);
      break;
    }
  case 2:
    {
      dim3 G( (N + BLK-1)/BLK, (M + BLK-1)/BLK);
      dim3 TB(BLK,BLK);
      
      mxmKernelV02 <<< G, TB >>> (M, K, N, A, B, C);
      break;
    }
  case 3:
    {
      dim3 G( (N + p_BN-1)/p_BN, (M + p_BM-1)/p_BM);
      dim3 TB(p_TN*p_TM);

//      std::cout << "TB.x=" << TB.x << ", TB.y=" << TB.y << ", G.x=" << G.x << ", G.y=" << G.y << std::endl;
      
      mxmKernelV03 <<< G, TB >>> (M, K, N, A, B, C);
      break;
    }
  case 4:
    {
      dim3 G( (N + p_BN-1)/p_BN, (M + p_BM-1)/p_BM);
      dim3 TB(p_TN*p_TM);
      
      mxmKernelV04 <<< G, TB >>> (M, K, N, A, B, C);
      break;
    }
  case 5:
    {
      dim3 G( (N + p_BN-1)/p_BN, (M + p_BM-1)/p_BM);
      dim3 TB(p_TN*p_TM);
      
      mxmKernelV05 <<< G, TB >>> (M, K, N, A, B, C);
      break;
    }
  case 6:
    {
      dim3 G( (N + p_BN-1)/p_BN, (M + p_BM-1)/p_BM);
      dim3 TB(p_TN*p_TM);
    
      mxmKernelV06 <<< G, TB >>> (M, K, N, A, B, C);
      break;

    }
  case 7:
    {
      dim3 G( (N + p_BN-1)/p_BN, (M + p_BM-1)/p_BM);
      dim3 TB(p_TN*p_TM);
    
      mxmKernelV07 <<< G, TB >>> (M, K, N, A, B, C);
      break;

    }
  case 8:
    {
      dim3 G( (N + p_BN-1)/p_BN, (M + p_BM-1)/p_BM);
      dim3 TB(p_TN*p_TM);
    
      mxmKernelV08 <<< G, TB >>> (M, K, N, A, B, C);
      break;

    }
  case 9:
    {
      dim3 G( (N + p_BN-1)/p_BN, (M + p_BM-1)/p_BM);
      dim3 TB(p_TN*p_TM);
    
      mxmKernelV09 <<< G, TB >>> (M, K, N, (dfloat4*)A, (dfloat4*)B, (dfloat4*)C);
      break;

    }
  case 10:
    {
      dim3 G( (N + p_BN-1)/p_BN, (M + p_BM-1)/p_BM);
      dim3 TB(p_TN*p_TM);
    
      mxmKernelV10 <<< G, TB >>> (M, K, N, A, B, C);
      break;

    }
  case 11:
    {
      dim3 blockDim(NUM_THREADS);
      
      constexpr uint NUM_WARPS = NUM_THREADS / 32;
      constexpr uint WMITER =
	(WM * WN) / (32 * TM * TN * WNITER);
      
      dim3 gridDim(CEIL_DIV(M, BN), CEIL_DIV(N, BM));
      
      mxmKernelVINFTY<<< gridDim, blockDim >>> (M, K, N, A, B, C);
      break;
    }
  default:
    {
      const dfloat alpha = 1.0, beta = 0.0;
      
      // C = beta*C + alpha*A*B
      cublasSgemm(handle,
		  CUBLAS_OP_N,
		  CUBLAS_OP_N,
		  N, M, K,
		  &alpha,
		  B, N,
		  A, K,
		  &beta,
		  C, N);
      break;
    }
  }
  

}

void cudaRandMalloc(int N, dfloat **h_A, dfloat **c_A){

  *h_A = (dfloat*) calloc(N, sizeof(dfloat));

  for(int n=0;n<N;++n){
    (*h_A)[n] = (drand48()-.5);
  }
  
  cudaMalloc(c_A, sizeof(dfloat)*N);
  cudaMemcpy(*c_A, *h_A, sizeof(dfloat)*N, cudaMemcpyHostToDevice);

}


int main(int argc, char **argv){

  int M = atoi(argv[1]);
  int K = atoi(argv[2]);
  int N = atoi(argv[3]);

  if((p_BM*p_BK)%(p_TM*p_TN)){
    printf("Incompatible config: p_BM=%d, p_BK=%d, p_TM=%d, p_TN=%d\n", p_BM, p_BK, p_TM, p_TN);
    exit(-1);
  }
  if((p_BN*p_BK)%(p_TM*p_TN)){
    printf("Incompatible config: p_BN=%d, p_BK=%d, p_TM=%d, p_TN=%d\n", p_BN, p_BK, p_TM, p_TN);
    exit(-1);
  }
  
  if(N%p_BN){
    printf("N=%d not divisible by p_BN=%d\n", N, p_BN);
    exit(-1);
  }

  if(M%p_BM){
    printf("M=%d not divisible by p_BM=%d\n", M, p_BM);
    exit(-1);
  }

  if(K%p_BK){
    printf("K=%d not divisible by p_BK=%d\n", K, p_BK);
    exit(-1);
  }

  dfloat *h_A, *h_B, *h_C;
  dfloat *c_A, *c_B, *c_C;

  int Nversion = 12;

  dfloat *h_saveC[Nversion];

  for(int v=0;v<Nversion;++v){
    h_saveC[v] = (dfloat*) calloc(M*N, sizeof(dfloat));
  }
  
  cudaRandMalloc(M*K, &h_A, &c_A);
  cudaRandMalloc(K*N, &h_B, &c_B);
  cudaRandMalloc(M*N, &h_C, &c_C);

  float elapsed;
  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cublasStatus_t stat; // CUBLAS functions status
  cublasHandle_t handle; // CUBLAS context
  stat = cublasCreate(&handle); // initialize CUBLAS context

  
  for(int version=1;version<=Nversion;++version){
{
    cudaMemset(c_C, 0, M*N*sizeof(dfloat));
    
    // warm up
    mxm(handle, version, M, K, N, c_A, c_B, c_C);

    cudaMemset(c_C, 0, M*N*sizeof(dfloat));
    
    cudaEventRecord(tic);
    mxm(handle, version, M, K, N, c_A, c_B, c_C);
    cudaEventRecord(toc);
    cudaDeviceSynchronize();

    cudaMemcpy(h_saveC[version-1], c_C, M*N*sizeof(dfloat), cudaMemcpyDeviceToHost);
    
    cudaEventElapsedTime(&elapsed, tic, toc);
    elapsed /= 1000.f;

    double TB = (sizeof(dfloat)*(M*K + K*N))/1.e12;
    double TBPS = TB/elapsed;
    double TFLOPS = (M*K)*(N/1.e12)*2./(elapsed);
    
    printf("%d, %d,%d,%d,%d,%d,  %g, %g, %g %%%% Kernel, TM, TN, BM, BN, BK, elapsed, TB/s, TFLOPS, \n",
	   version, p_TM, p_TN, p_BM, p_BN, p_BK, elapsed, TBPS, TFLOPS);
  }
}

#if 1
  // compare all outputs
  for(int k1=1;k1<=1;++k1){
    for(int k2=k1+1;k2<=Nversion;++k2){
      dfloat res = 0, sum = 0;
      for(int n=0;n<M*N;++n){
	res = std::max(res, fabs(h_saveC[k1-1][n]-h_saveC[k2-1][n]));
	sum +=fabs(h_saveC[k1-1][n]); 
      }
      std::cout << "k1=" << k1 << ", k2=" << k2 << ", diff=" << res << ", sum=" << sum << std::endl;
    }
  }
#endif
  
}
