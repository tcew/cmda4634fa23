
#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>

#define p_T 1024

// v0: horrible serial code
__global__ void reductionKernelV00(int N, float *x, float *sumx){

  // sum up N values from x
  float sum = 0;
  for(int n=0;n<N;++n){
    sum += x[n];
  }

  sumx[0] = sum;

}

// v1: half a vector  (run this log2(N) times)
__global__ void reductionKernelV01(int N, int M, float *x, float *sumx){

  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N && n<M && n+M<N){
    sumx[n] = x[n] + x[n+M];
  }
  
}

// v2: partial reduction by one thread using parallel loaded data
__global__ void reductionKernelV02(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t==0){
    float sum = 0;
    for(int m=0;m<p_T;++m){
      sum += s_x[m];
    }
    sumx[blockIdx.x] = sum;
  }
  
}


// v3: partial reduction by one thread using parallel loaded data
__global__ void reductionKernelV03(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t==0){
    float sum = 0;
    for(int m=0;m<p_T;++m){
      sum += s_x[m];
    }
    sumx[blockIdx.x] = sum;
  }
  
}

// v4: partial reduction by one thread using parallel loaded data
__global__ void reductionKernelV04(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t<512) s_x[t] += s_x[t+512];

  __syncthreads();

  if(t<256) s_x[t] += s_x[t+256];
  
  __syncthreads();

  if(t<128) s_x[t] += s_x[t+128];

  __syncthreads();

  if(t<64) s_x[t] += s_x[t+64];

  __syncthreads();

  if(t<32) s_x[t] += s_x[t+32];

  __syncthreads();

  if(t<16) s_x[t] += s_x[t+16];

  __syncthreads();
  
  if(t<8) s_x[t] += s_x[t+8];

  __syncthreads();

  if(t<4) s_x[t] += s_x[t+4];

  __syncthreads();

  if(t<2) s_x[t] += s_x[t+2];

  __syncthreads();

  if(t<1){
    s_x[t] += s_x[t+1];
    sumx[blockIdx.x] = s_x[0];
  }
}


// v5: throw away some synchronizations
__global__ void reductionKernelV05(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  volatile __shared__ float s_x[p_T]; 

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    s_x[t] = x[n];
  }

  // barrier all threads in this thread-block
  __syncthreads();

  if(t<512) s_x[t] += s_x[t+512];

  __syncthreads();

  if(t<256) s_x[t] += s_x[t+256];
  
  __syncthreads();

  if(t<128) s_x[t] += s_x[t+128];

  __syncthreads();

  if(t<64) s_x[t] += s_x[t+64];

  __syncthreads();

  if(t<32) s_x[t] += s_x[t+32];

  if(t<16) s_x[t] += s_x[t+16];
  
  if(t<8) s_x[t] += s_x[t+8];

  if(t<4) s_x[t] += s_x[t+4];

  if(t<2) s_x[t] += s_x[t+2];

  if(t<1){
    s_x[t] += s_x[t+1];
    sumx[blockIdx.x] = s_x[0];
  }
}



// v6: only use warp sync and one thread-block sync
__global__ void reductionKernelV06(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  volatile __shared__ float s_x[32][32];
  volatile __shared__ float s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = threadIdx.x + 32*w + blockIdx.x*1024;

  if(n<N){
    s_x[w][t] = x[n];
  }

  // barrier all threads in this thread-block
  if(t<16) s_x[w][t] += s_x[w][t+16];
  if(t< 8) s_x[w][t] += s_x[w][t+8];
  if(t< 4) s_x[w][t] += s_x[w][t+4];
  if(t< 2) s_x[w][t] += s_x[w][t+2];
  if(t< 1) s_x[w][t] += s_x[w][t+1];

  if(t==0){
    s_sumx[t] = s_x[w][0];
  }
  
  __syncthreads();

  if(w==0){
    if(t<16) s_sumx[t] += s_sumx[t+16];
    if(t<8)  s_sumx[t] += s_sumx[t+8];
    if(t<4)  s_sumx[t] += s_sumx[t+4];
    if(t<2)  s_sumx[t] += s_sumx[t+2];
    if(t<1)  s_sumx[t] += s_sumx[t+1];

    if(t==0){
      sumx[blockIdx.x] = s_sumx[0];
    }
  }
}


// v7: use warpsync
__global__ void reductionKernelV07(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[32][32];
  __shared__ float s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = threadIdx.x + 32*w + blockIdx.x*1024;

  if(n<N){
    s_x[w][t] = x[n];
  }

  // barrier all threads in this thread-block
  if(t<16) s_x[w][t] += s_x[w][t+16];
  __syncwarp();
  if(t< 8) s_x[w][t] += s_x[w][t+8];
  __syncwarp();
  if(t< 4) s_x[w][t] += s_x[w][t+4];
  __syncwarp();
  if(t< 2) s_x[w][t] += s_x[w][t+2];
  __syncwarp();
  if(t< 1) s_x[w][t] += s_x[w][t+1];
  __syncwarp();

  if(t==0){
    s_sumx[t] = s_x[w][0];
  }
  
  __syncthreads();

  if(w==0){
    if(t<16) s_sumx[t] += s_sumx[t+16];
    __syncwarp();
    if(t<8)  s_sumx[t] += s_sumx[t+8];
    __syncwarp();
    if(t<4)  s_sumx[t] += s_sumx[t+4];
    __syncwarp();    
    if(t<2)  s_sumx[t] += s_sumx[t+2];
    __syncwarp();
    if(t<1)  s_sumx[t] += s_sumx[t+1];

    if(t==0){
      sumx[blockIdx.x] = s_sumx[0];
    }
  }
}


// v8: pipeline data loads at start
__global__ void reductionKernelV08(int N, float *x, float *sumx){

  // guaranteed cache space on the SM running this thread-block
  __shared__ float s_x[32][32];
  __shared__ float s_sumx[32];

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = t + 32*w + blockIdx.x*1024;

  float tmp = 0;

  int m = n;
  while(m<N){
    tmp += x[m];
    m += 1024*gridDim.x; // number of threads
  }
  s_x[w][t] = tmp;
  __syncwarp();
  
  // barrier all threads in this thread-block
  if(t<16) s_x[w][t] += s_x[w][t+16];
  __syncwarp();
  if(t< 8) s_x[w][t] += s_x[w][t+8];
  __syncwarp();
  if(t< 4) s_x[w][t] += s_x[w][t+4];
  __syncwarp();
  if(t< 2) s_x[w][t] += s_x[w][t+2];
  __syncwarp();
  if(t< 1) s_x[w][t] += s_x[w][t+1];
  __syncwarp();

  if(t==0){
    s_sumx[t] = s_x[w][0];
  }
  
  __syncthreads();

  if(w==0){
    if(t<16) s_sumx[t] += s_sumx[t+16];
    __syncwarp();
    if(t<8)  s_sumx[t] += s_sumx[t+8];
    __syncwarp();
    if(t<4)  s_sumx[t] += s_sumx[t+4];
    __syncwarp();    
    if(t<2)  s_sumx[t] += s_sumx[t+2];
    __syncwarp();
    if(t<1)  s_sumx[t] += s_sumx[t+1];

    if(t==0){
      sumx[blockIdx.x] = s_sumx[0];
    }
  }
}

// atomic
__global__ void reductionKernelV09(int N, float *x, float *sumx){

  int t = threadIdx.x; // thread indexing in warp
  int w = threadIdx.y; // warp indexing
  int n = t + 32*w + blockIdx.x*1024;

  if(n<N){
    //    s_x[t] = x[n];
    int m = n;
    float tmp = 0;
    while(m<N){
      tmp += x[m];
      m += blockDim.x*blockDim.y*gridDim.x;
    }
    atomicAdd(sumx, tmp);
  }
}
  
void reductionLauncher(int knl, int N, float *h_x, float *c_x, float *c_sumx, float *h_sumx){

  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  
  cudaEventRecord(tic);

  for(int test=0;test<10;++test)
  switch(knl){
  case 0:{
    // one thread
    dim3 G(1), B(1); 
    reductionKernelV00 <<< G, B >>> (N, c_x, c_sumx);
    break;
  }
  case 1:{

    int M = N;
    int cnt = 0;
    
    while(M>p_T){    
      int G = (M+p_T-1)/p_T;
      
      if(G>0){
	if(cnt%2==0)
	  reductionKernelV01 <<< G, p_T >>> (N, M, c_x, c_sumx);
	else
	  reductionKernelV01 <<< G, p_T >>> (N, M, c_sumx, c_x);
      }
      M = (M+1)/2;
      ++cnt;
    }

#if 0
    if(cnt%2==0){ // result is in c_x;
    }
    else{ // result in c_sumx
    }
#endif
    break;
  }
  case 2:{
    // one thread
    dim3 G((N+p_T-1)/p_T), B(p_T); 
    reductionKernelV02 <<< G, B >>> (N, c_x, c_sumx);
    break;
  }
  case 3:{
    // one thread
    dim3 G((N+p_T-1)/p_T), B(p_T); 
    reductionKernelV03 <<< G, B >>> (N, c_x, c_sumx);
    break;
  }
  case 4:{
    // one thread
    dim3 G((N+p_T-1)/p_T), B(p_T); 
    reductionKernelV04 <<< G, B >>> (N, c_x, c_sumx);
    break;
  }
  case 5:{
    // one thread
    dim3 G((N+p_T-1)/p_T), B(p_T); 
    reductionKernelV05 <<< G, B >>> (N, c_x, c_sumx);
    break;
  }
  case 6:{
    dim3 G((N+1024-1)/1024), B(32,32); 
    reductionKernelV06 <<< G, B >>> (N, c_x, c_sumx);
    break;
  }
  case 7:{
    dim3 G((N+1024-1)/1024), B(32,32); 
    reductionKernelV07 <<< G, B >>> (N, c_x, c_sumx);
    break;
  }
  case 8:{
    int M = N; (N+15)/16;
    dim3 G((M+1024-1)/1024,1), B(32,32); 
    reductionKernelV08 <<< G, B >>> (N, c_x, c_sumx);

#if 0
    cudaMemcpy(h_sumx, c_sumx, G.x*sizeof(float), cudaMemcpyDeviceToHost);

    for(int n=1;n<G.x;++n){
      //      printf("h_sumx[%d]=%g\n", n, h_sumx[n]);
      h_sumx[0]+=h_sumx[n];
    }
    //    printf("G.x = %d,  h_sumx[0]=%f\n", G.x, h_sumx[0]);
#endif
    
    break;
  }
  case 9:{
    int M = (N+15)/16;
    dim3 G((M+1024-1)/1024), B(32,32);
    // zero out accumulator
    cudaMemset(c_sumx, 0, sizeof(float));
    reductionKernelV09 <<< G, B >>> (N, c_x, c_sumx);

    break;
  }
  }

  cudaEventRecord(toc);

  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.f;
  
  printf("kernel %02d took %e s ", knl, elapsed);

  if(knl==0){
    float h_sumx[1] = {0};
    cudaMemcpy(h_sumx, c_sumx, 1*sizeof(float), cudaMemcpyDeviceToHost);
    printf(" with sum %e\n", h_sumx[0]);
  }else{
    printf("\n");
  }
}

int main(int argc, char **argv){

  int N = atoi(argv[1]);

  float *h_x = (float*) calloc(N, sizeof(float));
  float *h_sumx = (float*) calloc(N, sizeof(float));

  for(int n=0;n<N;++n){
    h_x[n] = 1;
  }
  
  float *c_x, *c_sumx;
  cudaMalloc(&c_x,    N*sizeof(float));
  cudaMalloc(&c_sumx, N*sizeof(float));

  int Nknl = 10;

  for(int knl=0;knl<Nknl;++knl){
    reductionLauncher(knl, N, h_x, c_x, c_sumx, h_sumx);
  }


  
  
}
