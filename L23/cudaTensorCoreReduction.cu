// https://stackoverflow.com/questions/52831987/how-to-use-wmma-functions
#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>
#include <iostream>

/*
  1. THIS CODE CAN ONLY BE RUN ON VOLTA AND LATER GENERATION GPUS
  2. YOU CAN USE a V100 GPU ON THE ARC cascades CLUSTER !
  nvcc -arch=sm_70 -o cudaTensorUnitsIntro cudaTensorUnitsIntro.cu
 */

using namespace nvcuda;

// the wmma performs: D = A*B + C where
// 1. A is M x K
// 2. B is K x N
// 3. C,D are M x N
// see the docs for allowed precisions:
// https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html
#if 0
#define DIM_M 16
#define DIM_N 16
#define DIM_K 16
#endif
#if 1
// number of rows of A (and of C)
#define DIM_M 8
// numner of columns of B (and of C)
#define DIM_N 32
// number of columns of A and also rows of B
#define DIM_K 16
#endif

#define MASK 0xffffffff
#define WIDTH 32


__global__ void tensorCoreReductionKernelV0(int N, half *c_halfX, float *c_reduceX){

  int e = blockIdx.x;
  int t = threadIdx.x;
  
  // use same A for all thread-blocks, different B and C
  half  *Xe = c_halfX + DIM_K*DIM_N*e;

  // Declare the fragments
  wmma::fragment<wmma::matrix_a,    DIM_M, DIM_N, DIM_K, half, wmma::row_major> Ae_frag;
  wmma::fragment<wmma::matrix_b,    DIM_M, DIM_N, DIM_K, half, wmma::col_major> Be_frag;
  wmma::fragment<wmma::accumulator, DIM_M, DIM_N, DIM_K, float> Ce_frag;

  __shared__ half s_A[DIM_M*DIM_K];
  __shared__ float s_reducedA[DIM_M*DIM_N];

  if(t<DIM_K)
    s_A[t] = (half)1.f; // just load up first row
  
  // Load the inputs
  wmma::load_matrix_sync(Ae_frag, s_A, DIM_K);
  wmma::load_matrix_sync(Be_frag, Xe,  DIM_K);

  // zero accumulator
  wmma::fill_fragment(Ce_frag, 0.0f);

  // Perform the matrix multiplication  ( Ce_frag = Ae_frag*Be_frag )
  wmma::mma_sync(Ce_frag, Ae_frag, Be_frag, Ce_frag);

  // Ce_frag => Ze (need to store accumulator to copy it to Be)
  wmma::store_matrix_sync(s_reducedA, Ce_frag, DIM_N, wmma::mem_row_major);
  
  // now reduce to one value
  // DIMZ (16) entries left in s_A
  float r_val = (t<DIM_N) ? s_reducedA[t]:0;
  float tmp;
  tmp = __shfl_down_sync(MASK, r_val, 16, WIDTH);
  r_val += (t<16 && t+16<DIM_N) ? tmp: 0.f;
  tmp = __shfl_down_sync(MASK, r_val, 8, WIDTH);
  r_val += (t<8) ? tmp: 0.f;
  tmp = __shfl_down_sync(MASK, r_val, 4, WIDTH);
  r_val += (t<4) ? tmp: 0.f;
  tmp = __shfl_down_sync(MASK, r_val, 2, WIDTH);
  r_val += (t<2) ? tmp: 0.f;
  tmp = __shfl_down_sync(MASK, r_val, 1, WIDTH);
  r_val += (t<1) ? tmp: 0.f;
  if(t==0){
    atomicAdd(c_reduceX, r_val);
  }
}


// use 2 warps
__global__ void tensorCoreReductionKernelV1(int N, half *c_halfX, float *c_reduceX){

  int eo = blockIdx.x;
  int es = threadIdx.y;
  int t = threadIdx.x;
  int e = eo*blockDim.y+es;
  
  // use same A for all thread-blocks, different B and C
  half  *Xe = c_halfX + DIM_K*DIM_N*e;

  // Declare the fragments
  wmma::fragment<wmma::matrix_a,    DIM_M, DIM_N, DIM_K, half, wmma::row_major> Ae_frag;
  wmma::fragment<wmma::matrix_b,    DIM_M, DIM_N, DIM_K, half, wmma::col_major> Be_frag;
  wmma::fragment<wmma::accumulator, DIM_M, DIM_N, DIM_K, float> Ce_frag;

  __shared__ half s_A[DIM_M*DIM_K];
  __shared__ float s_reducedA[2][DIM_M*DIM_N];

  if(t<DIM_K && es==0)
    s_A[t] = (half)1.f; // just load up first row

  __syncthreads();
  
  // Load the inputs
  wmma::load_matrix_sync(Ae_frag, s_A, DIM_K);
  if(e<N)
    wmma::load_matrix_sync(Be_frag, Xe,  DIM_K);

  // zero accumulator
  wmma::fill_fragment(Ce_frag, 0.0f);

  // Perform the matrix multiplication  ( Ce_frag = Ae_frag*Be_frag )
  wmma::mma_sync(Ce_frag, Ae_frag, Be_frag, Ce_frag);

  // Ce_frag => Ze (need to store accumulator to copy it to Be)
  wmma::store_matrix_sync(s_reducedA[es], Ce_frag, DIM_N, wmma::mem_row_major);
  
  // now reduce to one value
  // DIMZ (16) entries left in s_A
  float r_val = (t<DIM_N) ? s_reducedA[es][t]:0;
  float tmp;
  tmp = __shfl_down_sync(MASK, r_val, 16, WIDTH);
  r_val += (t<16 && t+16<DIM_N) ? tmp: 0.f;
  tmp = __shfl_down_sync(MASK, r_val, 8, WIDTH);
  r_val += (t<8) ? tmp: 0.f;
  tmp = __shfl_down_sync(MASK, r_val, 4, WIDTH);
  r_val += (t<4) ? tmp: 0.f;
  tmp = __shfl_down_sync(MASK, r_val, 2, WIDTH);
  r_val += (t<2) ? tmp: 0.f;
  tmp = __shfl_down_sync(MASK, r_val, 1, WIDTH);
  r_val += (t<1) ? tmp: 0.f;
  if(t==0 && e<N){
    atomicAdd(c_reduceX, r_val);
  }
}


__global__ void float2HalfKernel(int N, float *a, half *b){
	   
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    b[n] = __float2half(a[n]);
  }
}

#define DIM2 256

void float2Half(int N, float *c_a, half *c_halfa){
  int T = DIM2;
  dim3 B(T);
  dim3 G((N+T-1)/T);
  
  float2HalfKernel<<<G,B>>>(N, c_a, c_halfa);
}

__global__ void half2FloatKernel(int N, half *a, float *b){
	   
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    b[n] = __half2float(a[n]);
  }
}



void half2Float(int N, half *c_halfa, float *c_a){
  int T = DIM2;
  dim3 B(T);
  dim3 G((N+T-1)/T);
  
  half2FloatKernel<<<G,B>>>(N, c_halfa, c_a);
}

void runKernel(int op, int N, half *c_halfA, float *c_reducedA){

  cudaMemset(c_reducedA, 0, sizeof(float));
  
  if(op==0)
    tensorCoreReductionKernelV0 <<<(N+DIM_K*DIM_N-1)/(DIM_K*DIM_N),32>>>(N, c_halfA, c_reducedA);
  if(op==1)
    tensorCoreReductionKernelV1 <<<(N+2*DIM_K*DIM_N-1)/(2*DIM_K*DIM_N),dim3(32,2)>>>(N, c_halfA, c_reducedA);
  
}

double timeKernel(int op, int N, half *c_halfA, float *c_reducedA){
  
  // warm up
  runKernel(op, N, c_halfA, c_reducedA);
  cudaDeviceSynchronize();
    
  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  
  runKernel(op, N, c_halfA, c_reducedA);
    
  cudaEventRecord(toc);
  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.;

  double gbytes = N*sizeof(half)/1.e9;
  
  return gbytes/elapsed;
}

int main(int argc, char **argv){

  if(argc!=2) { printf("usage: ./cudaTensorUnitsIntro N\n"); exit(-1); }

  int N = atoi(argv[1]);

  // round up to multiple of DIMY*DIMZ*2
  N = 2*DIM_K*DIM_N*( (N+2*DIM_K*DIM_N-1)/(2*DIM_K*DIM_N) );
  printf("Using N=%d\n", N);
  
  float *h_X, *c_X, *c_reducedX, *h_reducedX;
  half *c_halfX;

  h_X = new float[N];
  h_reducedX = new float[1];
  
  // create arrays for X,Y,D
  cudaMalloc(&c_X, N*sizeof(float));
  cudaMalloc(&c_halfX, N*sizeof(half));
  cudaMalloc(&c_reducedX, 1*sizeof(float));
  
  // build matrices such that 
  for(int n=0;n<N;++n){
    h_X[n] = 1.0;
  }
  
  cudaMemcpy(c_X, h_X, N*sizeof(float), cudaMemcpyHostToDevice);

  // convert float to half
  float2Half(N, c_X, c_halfX);

  // time two variants
  double bandwidth0 = timeKernel(0, N, c_halfX, c_reducedX);

  cudaMemcpy(h_reducedX, c_reducedX, sizeof(float), cudaMemcpyDeviceToHost);
  
  printf("V0: throughput %g GB/s\n", bandwidth0);
  printf("V0: reduced sum %g\n", h_reducedX[0]);
  printf("V0: GB %g\n", N*sizeof(half)/1.e9);
    
  double bandwidth1 = timeKernel(1, N, c_halfX, c_reducedX);

  cudaMemcpy(h_reducedX, c_reducedX, sizeof(float), cudaMemcpyDeviceToHost);

  printf("V1: throughput %g GB/s\n", bandwidth1);
  printf("V1: reduced sum %g\n", h_reducedX[0]);
}
