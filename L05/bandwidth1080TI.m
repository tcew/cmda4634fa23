data = [
2.048000e-05,7.168000e-06, 2.857143e+00 %% gigabytes, achievedBandwidth (GB/s)
2.048000e-04,6.144000e-06, 3.333333e+01 %% gigabytes, achievedBandwidth (GB/s)
2.048000e-03,7.168000e-06, 2.857143e+02 %% gigabytes, achievedBandwidth (GB/s)
2.048000e-02,6.144000e-05, 3.333333e+02 %% gigabytes, achievedBandwidth (GB/s)
2.048000e-01,5.713920e-04, 3.584229e+02 %% gigabytes, achievedBandwidth (GB/s)
2.048000e+00,5.663744e-03, 3.615983e+02 %% gigabytes, achievedBandwidth (GB/s)];
];

data = [
1.024000e-05,1.228800e-05, 8.333333e-01 %% gigabytes, achievedBandwidth (GB/s)
1.024000e-04,6.144000e-06, 1.666667e+01 %% gigabytes, achievedBandwidth (GB/s)
1.024000e-03,6.144000e-06, 1.666667e+02 %% gigabytes, achievedBandwidth (GB/s)
1.024000e-02,3.072000e-05, 3.333333e+02 %% gigabytes, achievedBandwidth (GB/s)
1.024000e-01,2.621440e-04, 3.906250e+02 %% gigabytes, achievedBandwidth (GB/s)
1.024000e+00,2.580480e-03, 3.968254e+02 %% gigabytes, achievedBandwidth (GB/s)    
];

Ndata = size(data,1);

plot(data(:,1), data(:,2), 'r-o')
xlabel('B (GB)', 'fontsize', 20)
ylabel('T_B (GB/s)', 'fontsize', 20)
set(gcf,'color', 'w')

B = data(:,1);
TB = data(:,2);

A = [ones(Ndata,1), B];
coeffs = A\TB;

T0 = coeffs(1)
Rinf = 1/coeffs(2)

Bout = linspace(1e-7, 4, 100000);
Tout = T0 + Bout/Rinf;
hold on 
plot(Bout, Tout, 'k-')
hold off

figure(2)
Wout = Bout./(T0 + Bout./Rinf);
loglog(Bout, Wout)
hold on
semilogy(B, data(:,3), 'r*')
hold off
xlabel('GB', 'fontsize', 20)
ylabel('Bandwidth', 'fontsize', 20)


