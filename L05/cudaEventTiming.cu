
#include <stdio.h>
#include <time.h>

__global__
void deviceCopy(int N, float *x, float *y){
  
  int t  = threadIdx.x; // in the range 0 to number of threads per thread-block
  int b  = blockIdx.x;  // in the range 0 to number of blocks-1
  int d  = blockDim.x;  // how many threads in each thread-block

  //  unique map between thread index, block index, and block size and integers
  int n = b*d + t;

  if(n<N)
    y[n] = x[n];
  
}


void hostCopy(int N, float *x, float *y){

  for(int n=0;n<N;++n){
    y[n] = x[n];
  }
  
}

int main(int argc, char **argv){

  if(argc<3){
    printf("usage: ./cudaEventTiming NumberThreadBlocks ThreadsPerThreadBlock\n");
    exit(-1);
  }

  int G = atoi(argv[1]);
  int T = atoi(argv[2]);

  int N = G*T;

  float *h_x;
  float *c_x, *c_y;

  // allocate arrays on HOST
  h_x = (float*) malloc(N*sizeof(float));

  for(int n=0;n<10;++n){
    h_x[n] = n;
  }
  
  // allocate array on DEVICE
  cudaMalloc(&c_x, N*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));

  // copy data from HOST to DEVICE
  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);

  deviceCopy <<< G , T  >>> (N, c_x, c_y);

  
  // launch kernel
  clock_t tic, toc;

  cudaDeviceSynchronize();
  tic = clock();
  deviceCopy <<< G , T  >>> (N, c_x, c_y);
  cudaDeviceSynchronize();
  toc = clock();
  
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  cudaEventRecord(start);
  deviceCopy <<< G , T  >>> (N, c_x, c_y);
  cudaEventRecord(stop);

  cudaDeviceSynchronize();

  float deviceElapsed;
  cudaEventElapsedTime(&deviceElapsed, start, stop);
  deviceElapsed = deviceElapsed/1000.;
  
  double elapsed = (toc-tic)/(double)CLOCKS_PER_SEC;
  
  printf("elapsed = %e\n",  elapsed);
  printf("deviceElapsed = %e\n",  deviceElapsed);

  double gigabytes = N*2*sizeof(float)/1.e9;

  double achievedBandwidth = gigabytes/deviceElapsed; 

  printf("%e,%e, %e %%%% gigabytes, achievedBandwidth (GB/s)\n",
  	 gigabytes, deviceElapsed, achievedBandwidth);
  
  // copy data from DEVICE to HOST
  cudaMemcpy(h_x, c_y, N*sizeof(float), cudaMemcpyDeviceToHost);

  
  cudaFree(c_x);
  cudaFree(c_y);

  free(h_x);
  
}
