#include <cuda.h>


#define BLOCKSIZE 1024

__global__ void scanKernel(int N, float *x, float *y){

  __shared__ float s_x[BLOCKSIZE];

  int t = threadIdx.x;
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  s_x[t] = x[n];

  __syncthreads();

  if(t>=1) {s_x[t] += s_x[t-1];}   __syncthreads();
  if(t>=2){s_x[t] += s_x[t-2];}   __syncthreads();
  if(t>=4){s_x[t] += s_x[t-4];}   __syncthreads();
  if(t>=8){s_x[t] += s_x[t-8];}   __syncthreads();
  if(t>=16){s_x[t] += s_x[t-16];}   __syncthreads();
  if(t>=32){s_x[t] += s_x[t-32];}   __syncthreads();
  if(t>=64){s_x[t] += s_x[t-64];}   __syncthreads();
  if(t>=128){s_x[t] += s_x[t-128];}   __syncthreads();
  if(t>=256){s_x[t] += s_x[t-256];}   __syncthreads();
  if(t>=512){s_x[t] += s_x[t-512];}

  y[n] = s_x[t];
}
