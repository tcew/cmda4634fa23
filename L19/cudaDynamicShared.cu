
#include <cuda.h>

__global__ void someKernel(int N, float *x, float *y){

  // dynamically sized shared memory array
  extern __shared__ float s_x[];

  __shared__ float *s_x1 = s_x;
  __shared__ float *s_x2 = s_x+32;
  
  int n = threadIdx.x + blockIdx.x*blockDim.x;
  s_x1[threadIdx.x] = x[n];
	   
}

int main(int argc, char **argv){

  
  someKernel <<< 100, 32, 64*sizeof(float) >>> (N, c_x, c_y);

}
