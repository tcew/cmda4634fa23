
#include <stdio.h>

__global__
void devicePrint(){

  printf("Hello world from GPU!!!\n");
  
}


void hostPrint(){

  printf("Hello world from CPU!!!\n");
  
}

int main(int argc, char **argv){

  hostPrint();

  devicePrint <<< 100 , 1024 >>> ();

  cudaDeviceSynchronize();
}
