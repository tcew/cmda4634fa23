
#include <stdio.h>
#include <stdlib.h>

#include <cuda.h>

/* function to convert from (i,j) cell index to linear storage index */
__host__ __device__ int idx(int N, int i, int j){
  int n = i + (N + 2)*j;
  return n;  
}

// number of threads will be p_T x p_T
#define p_T 32
#define mask 0xffffffff

/* serial reference function to update Inew from Iold */
void golUpdateSerial(int N, int *Iold, int *Inew){

  for(int i=1;i<=N;++i){ // notice the loop bounds are [1,N]
    for(int j=1;j<=N;++j){ // notice the loop bounds are [1,n]

      // sum up 8 neighbor values (total number of alive neighbors)
      int n = Iold[idx(N,i-1,j-1)] + Iold[idx(N,i+1,j-1)] + Iold[idx(N,i,j-1)]
	    + Iold[idx(N,i-1,j)]                          + Iold[idx(N,i+1,j)]
	    + Iold[idx(N,i,j+1)]   + Iold[idx(N,i-1,j+1)] + Iold[idx(N,i+1,j+1)];
      
      // distilled version
      int oldState = Iold[idx(N,i,j)];
      int newState = (oldState==1) ? ( (n==2)||(n==3) ) : (n==3) ;
      Inew[idx(N,i,j)] = newState;
    }
  }
}


int compareBoardsSerial(int N, int *Iold, int *Inew){
  int diff = 0;

  for(int i=1;i<=N;++i){ // notice the loop bounds are [1,N]
    for(int j=1;j<=N;++j){ // notice the loop bounds are [1,n]
      int id = idx(N,i,j);
      diff += abs(Iold[id]-Inew[id]);
    }
  }

  return diff;
}


__global__ void golUpdatePart1Kernel(int N, int *Iold, int *Inew){

  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  // assume:
  // 1. the board storage is of size (N+2)x(N+2), i.e. (0:N+1) x (0:N+1)
  // 2. we are updating (1:N) x (1:N)
  // 3. we have at least NxN threads
  // 4. this thread-block has (dx) x (dy) threads, but we are not going to use the outmost threads in the thread-block to update
  int i = by*(p_T-2) + ty;
  int j = bx*(p_T-2) + tx;

  if(tx>=1 && tx<=p_T-2 &&
     ty>=1 && ty<=p_T-2){

    int newState = 0;
    
    if(i>=1 && j>=1 && i<=N && j<=N){
      
      // sum up 8 neighbor values (total number of alive neighbors)
      int n = Iold[idx(N,i-1,j-1)] + Iold[idx(N,i+1,j-1)] + Iold[idx(N,i,j-1)]
	    + Iold[idx(N,i-1,j)]                          + Iold[idx(N,i+1,j)]
    	    + Iold[idx(N,i,j+1)]   + Iold[idx(N,i-1,j+1)] + Iold[idx(N,i+1,j+1)];
    
      // distilled version of the GoL test
      int oldState = Iold[idx(N,i,j)];
      newState = (oldState==1) ? ( (n==2)||(n==3) ) : (n==3) ;
    }
    
    if(i<N+2 && j<N+2){
      Inew[idx(N,i,j)] = newState;
    }
  }
}

void golUpdatePart1(int N, int *c_Iold, int *c_Inew){

  dim3 B(p_T, p_T);
  dim3 G( (N+2 + (p_T-2)-1)/(p_T-2), (N+2 +(p_T-2)-1)/(p_T-2));
  
  golUpdatePart1Kernel <<< G, B >>> (N, c_Iold, c_Inew);
  
}


// shared memory version
__global__ void golUpdatePart2Kernel(int N, int *Iold, int *Inew){

  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  // assume:
  // 1. the board storage is of size (N+2)x(N+2), i.e. (0:N+1) x (0:N+1)
  // 2. we are updating (1:N) x (1:N)
  // 3. we have at least NxN threads
  // 4. this thread-block has (dx) x (dy) threads, but we are not going to use the outmost threads in the thread-block to update
  // 5. all threads will load data into shared (if in bounds), only interior threads will write out (if in bounds)
  
  int i = by*(p_T-2) + ty;
  int j = bx*(p_T-2) + tx;
  
  __shared__ int s_Iold[p_T][p_T];

  // read if in bounds or zero
  s_Iold[ty][tx] = (i<N+2 && j<N+2) ? Iold[idx(N,i,j)]:0; 

  // make sure write to shared completed by this thread-block
  __syncthreads();
  
  if(tx>=1 && tx<=p_T-2 &&
     ty>=1 && ty<=p_T-2){
    
    int newState = 0;
    
    if(tx>=1 && ty>=1 && tx<=p_T-2 && ty<=p_T-2){
      
      // sum up 8 neighbor values (total number of alive neighbors)
      int n = s_Iold[ty-1][tx-1] + s_Iold[ty-1][tx] + s_Iold[ty-1][tx+1]
	     +s_Iold[ty][tx-1]   +                    s_Iold[ty][tx+1] 
	     +s_Iold[ty+1][tx-1] + s_Iold[ty+1][tx] + s_Iold[ty+1][tx+1];

      // distilled version of the GoL test
      int oldState = s_Iold[ty][tx];
      newState = (oldState==1) ? ( (n==2)||(n==3) ) : (n==3) ;
    }
    
    if(i>=1 && j>=1&& i<=N && j<=N){
      Inew[idx(N,i,j)] = newState;
    }
  }
}

void golUpdatePart2(int N, int *c_Iold, int *c_Inew){

  dim3 B(p_T, p_T);
  dim3 G( (N+2 + (p_T-2)-1)/(p_T-2), (N+2 +(p_T-2)-1)/(p_T-2));

  golUpdatePart2Kernel <<< G, B >>> (N, c_Iold, c_Inew);
  
}


// shared+shfl memory version
__global__ void golUpdatePart3Kernel(int N, int *Iold, int *Inew){

  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  // assume:
  // 1. the board storage is of size (N+2)x(N+2), i.e. (0:N+1) x (0:N+1)
  // 2. we are updating (1:N) x (1:N)
  // 3. we have at least NxN threads
  // 4. this thread-block has (dx) x (dy) threads, but we are not going to use the outmost threads in the thread-block to update
  // 5. all threads will load data into shared (if in bounds), only interior threads will write out (if in bounds)
  // 6. threads in the same warp work together to compute sums for groups of 3 cells 
  int i = by*(p_T-2) + ty;
  int j = bx*(p_T-2) + tx;
  
  __shared__ int s_Isum[p_T][p_T];

  // read if in bounds or zero
  int valC = (i<N+2 && j<N+2) ? Iold[idx(N,i,j)]:0;
  // get East, West neighbors by shfl
  int valE = __shfl_down_sync(mask, valC, 1);
  int valW = __shfl_up_sync  (mask, valC, 1);

  // add E,C,W values
  int sum = valC;
  sum += (tx<p_T-1) ? valE:0;
  sum += (tx>0)     ? valW:0;

  s_Isum[ty][tx] = sum;

  // make sure write to shared completed by this thread-block
  __syncthreads();
  
  if(tx>=1 && tx<=p_T-2 &&
     ty>=1 && ty<=p_T-2){
    
    int newState = 0;
    
    if(tx>=1 && ty>=1 && tx<=p_T-2 && ty<=p_T-2){
      // sum up 8 neighbor values (sums from north, south and center)
      int n = s_Isum[ty-1][tx] + s_Isum[ty][tx] + s_Isum[ty+1][tx] - valC;

      // distilled version of the GoL test
      int oldState = valC;
      newState = (oldState==1) ? ( (n==2)||(n==3) ) : (n==3) ;
    }
    
    if(i>=1 && j>=1&& i<=N && j<=N){
      Inew[idx(N,i,j)] = newState;
    }
  }
}

void golUpdatePart3(int N, int *c_Iold, int *c_Inew){

  dim3 B(p_T, p_T);
  dim3 G( (N+2 + (p_T-2)-1)/(p_T-2), (N+2 +(p_T-2)-1)/(p_T-2));

  golUpdatePart3Kernel <<< G, B >>> (N, c_Iold, c_Inew);
  
}


// use two-phase reduction to reduce board difference, and then update atomic accumulator
__global__ void compareBoardsKernel(int N, int *Iold, int *Inew, int *diff){

  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  
  int i = by*(p_T-2) + ty;
  int j = bx*(p_T-2) + tx;

  int id = idx(N,i,j);

  // only doing one load per thread (this can be optimized further)
  int d = (i>=1 && i<=N && j>=1 && j<=N && tx>=1 && tx<=p_T-2 && ty>=1 && ty<=p_T-2) ?  abs(Iold[id]-Inew[id]) : 0;

  __shared__ int s_d[p_T];
  
  // warp based sum
  int tmp = __shfl_down_sync(mask, d, 16);
  d += tmp;
  tmp = __shfl_down_sync(mask, d, 8);
  d += tmp;
  tmp = __shfl_down_sync(mask, d, 4);
  d += tmp;
  tmp = __shfl_down_sync(mask, d, 2);
  d += tmp;
  tmp = __shfl_down_sync(mask, d, 1);
  d += tmp;

  if(tx==0) { // root thread of warp
    s_d[ty] = d;
  }

  __syncthreads();
  
  if(ty==0){

    d = s_d[tx];
    
    tmp = __shfl_down_sync(mask, d, 16);
    d += tmp;
    tmp = __shfl_down_sync(mask, d, 8);
    d += tmp;
    tmp = __shfl_down_sync(mask, d, 4);
    d += tmp;
    tmp = __shfl_down_sync(mask, d, 2);
    d += tmp;
    tmp = __shfl_down_sync(mask, d, 1);
    d += tmp;

    if(tx==0){
      atomicAdd(diff, d);
    }
  }
}

int compareBoardsLauncher(int N, int *c_Iold, int *c_Inew){

  int *c_diff;
  cudaMalloc(&c_diff, sizeof(int));
  cudaMemset(c_diff, 0, sizeof(int));
  
  dim3 B(p_T, p_T);
  dim3 G( (N+2 + (p_T-2)-1)/(p_T-2), (N+2 +(p_T-2)-1)/(p_T-2));

  compareBoardsKernel <<< G, B >>> (N, c_Iold, c_Inew, c_diff);

  int diff;
  cudaMemcpy(&diff, c_diff, sizeof(int), cudaMemcpyDeviceToHost);

  return diff;
}

int main(int argc, char **argv){

  int N = atoi(argv[1]);
  int Ncells = (N+2)*(N+2);
  
  int *h_Iold  = (int*) calloc( Ncells, sizeof(int) );
  int *h_Inew0 = (int*) calloc( Ncells, sizeof(int) );
  int *h_Inew1 = (int*) calloc( Ncells, sizeof(int) );
  int *h_Inew2 = (int*) calloc( Ncells, sizeof(int) );
  int *h_Inew3 = (int*) calloc( Ncells, sizeof(int) );

  int *c_Iold;
  int *c_Inew1, *c_Inew2, *c_Inew3;

  cudaMalloc(&c_Iold,  Ncells*sizeof(int));
  cudaMalloc(&c_Inew1, Ncells*sizeof(int));
  cudaMalloc(&c_Inew2, Ncells*sizeof(int));
  cudaMalloc(&c_Inew3, Ncells*sizeof(int));

  cudaMemset(c_Iold,  0, Ncells*sizeof(int));
  cudaMemset(c_Inew1, 0, Ncells*sizeof(int));
  cudaMemset(c_Inew2, 0, Ncells*sizeof(int));
  cudaMemset(c_Inew3, 0, Ncells*sizeof(int));

  
  for(int i=1;i<=N;++i){
    for(int j=1;j<=N;++j){

      if(drand48()>.8){ // 20% fill
	h_Iold[idx(N, i, j)] = 1;
      }
    }
  }

  // let's warm up the GPU kernels
  int Nwarm = 10;
  for(int test=0;test<Nwarm;++test){
    golUpdatePart1(N, c_Iold, c_Inew1);
    golUpdatePart2(N, c_Iold, c_Inew2);
    golUpdatePart3(N, c_Iold, c_Inew3);
  }

  cudaEvent_t tic0, tic1, tic2, tic3;
  cudaEvent_t toc0, toc1, toc2, toc3;
  float elapsed0, elapsed1, elapsed2, elapsed3;

  cudaEventCreate(&tic0); cudaEventCreate(&tic1); cudaEventCreate(&tic2); cudaEventCreate(&tic3);
  cudaEventCreate(&toc0); cudaEventCreate(&toc1); cudaEventCreate(&toc2); cudaEventCreate(&toc3);
  
  // serial version (0)
  cudaEventRecord(tic0);
  golUpdateSerial(N, h_Iold, h_Inew0);
  cudaEventRecord(toc0);

  // copy old board to DEVICE
  cudaMemcpy(c_Iold, h_Iold, Ncells*sizeof(int), cudaMemcpyHostToDevice);
  
  // based kernel version (1)
  cudaEventRecord(tic1);
  golUpdatePart1(N, c_Iold, c_Inew1);
  cudaEventRecord(toc1);
  
  // shared memory kernel version (2)
  cudaEventRecord(tic2);  
  golUpdatePart2(N, c_Iold, c_Inew2);
  cudaEventRecord(toc2);

  // shared memory + shfl kernel version (3)
  cudaEventRecord(tic3);  
  golUpdatePart3(N, c_Iold, c_Inew3);
  cudaEventRecord(toc3);

  // copy boards back to HOST
  cudaMemcpy(h_Inew1, c_Inew1, Ncells*sizeof(int), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_Inew2, c_Inew2, Ncells*sizeof(int), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_Inew3, c_Inew3, Ncells*sizeof(int), cudaMemcpyDeviceToHost);

  // compare against board update from host
  int diffTestHost = compareBoardsSerial(N, h_Iold, h_Inew0);
  int diff1 = compareBoardsSerial(N, h_Inew0, h_Inew1);
  int diff2 = compareBoardsSerial(N, h_Inew0, h_Inew2);
  int diff3 = compareBoardsSerial(N, h_Inew0, h_Inew3);

  int diffTestDevice = compareBoardsLauncher(N, c_Iold, c_Inew2);
  int diff12 = compareBoardsLauncher(N, c_Inew1, c_Inew2);
  int diff13 = compareBoardsLauncher(N, c_Inew1, c_Inew3);
  int diff23 = compareBoardsLauncher(N, c_Inew2, c_Inew3);

  printf("diffTestHost=%d, diffTestDevice=%d\n", diffTestHost, diffTestDevice);
  printf("diff1=%d, diff2=%d, diff3=%d\n", diff1, diff2, diff3);
  printf("diff12=%d, diff13=%d, diff23=%d\n", diff12, diff13, diff23);

  cudaEventElapsedTime(&elapsed0, tic0, toc0); elapsed0 /= 1000.f;
  cudaEventElapsedTime(&elapsed1, tic1, toc1); elapsed1 /= 1000.f;
  cudaEventElapsedTime(&elapsed2, tic2, toc2); elapsed2 /= 1000.f;
  cudaEventElapsedTime(&elapsed3, tic3, toc3); elapsed3 /= 1000.f;  

  printf("HOST:  %g s\n", elapsed0);
  printf("PART1: %g s\n", elapsed1);
  printf("PART2: %g s\n", elapsed2);
  printf("PART3: %g s\n", elapsed3);
  
  cudaFree(c_Iold);
  cudaFree(c_Inew1);
  cudaFree(c_Inew2);
  cudaFree(c_Inew3);

  free(h_Iold);
  free(h_Inew0);
  free(h_Inew1);
  free(h_Inew2);
  free(h_Inew3);
}
